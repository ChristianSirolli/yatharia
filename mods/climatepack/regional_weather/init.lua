local modname = minetest.get_current_modname()

local function get_setting_bool(name, default, is_global)
	local prefix = ""
	if not is_global then prefix = "regional_weather_" end
	local value = minetest.settings:get_bool(prefix .. name)
	if type(value) == "nil" then value = default end
	return minetest.is_yes(value)
end

local function get_setting_number(name, default, is_global)
	local prefix = ""
	if not is_global then prefix = "regional_weather_" end
	local value = minetest.settings:get(prefix .. name)
	if type(value) == "nil" then value = default end
	return tonumber(value)
end

regional_weather = {
	i18n = minetest.get_translator("regional_weather"),
	path = minetest.get_modpath(modname),
	settings = {
		player_speed = get_setting_bool("player_speed", true),
		snow = get_setting_bool("snow_layers", true),
		snow_griefing = get_setting_bool("snow_griefing", true),
		puddles = get_setting_bool("puddles", true),
		puddles_water = get_setting_bool("puddles_water", false),
		soil = get_setting_bool("soil", true),
		fire = get_setting_bool("fire", true),
		ice = get_setting_bool("ice", true),
		pedology = get_setting_bool("pedology", true),
		lightning = get_setting_number("lightning", 1),
		max_height = get_setting_number("max_height", 120),
		min_height = get_setting_number("min_height", -50),
		cloud_height = get_setting_number("cloud_height", 120),
		cloud_scale = get_setting_number("cloud_scale", 40),
	}
}

-- warn about clouds being overriden by MTG weather
if climate_mod.settings.skybox
and minetest.global_exists("weather")
and get_setting_bool("enable_weather", true, true) then
	minetest.log("warning", "[Regional Weather] " .. regional_weather.i18n("Disable MTG weather for the best experience. Check the forum for more information."))
end

-- import individual weather types
dofile(regional_weather.path.."/ca_weathers/ambient.lua")
dofile(regional_weather.path.."/ca_weathers/deep_cave.lua")
dofile(regional_weather.path.."/ca_weathers/fog.lua")
dofile(regional_weather.path.."/ca_weathers/fog_heavy.lua")
dofile(regional_weather.path.."/ca_weathers/hail.lua")
dofile(regional_weather.path.."/ca_weathers/pollen.lua")
dofile(regional_weather.path.."/ca_weathers/rain.lua")
dofile(regional_weather.path.."/ca_weathers/rain_heavy.lua")
dofile(regional_weather.path.."/ca_weathers/sandstorm.lua")
dofile(regional_weather.path.."/ca_weathers/snow.lua")
dofile(regional_weather.path.."/ca_weathers/snow_heavy.lua")
dofile(regional_weather.path.."/ca_weathers/storm.lua")

-- register environment effects
dofile(regional_weather.path.."/ca_effects/lightning.lua")
dofile(regional_weather.path.."/ca_effects/speed_buff.lua")

-- register ABM cycles and custom nodes
dofile(regional_weather.path .. "/abms/puddle.lua")
dofile(regional_weather.path .. "/abms/snow_cover.lua")
dofile(regional_weather.path .. "/abms/fire.lua")
dofile(regional_weather.path .. "/abms/ice.lua")
dofile(regional_weather.path .. "/abms/pedology.lua")
dofile(regional_weather.path .. "/abms/soil.lua")
