dynamic_liquid = {
	path = minetest.get_modpath(minetest.get_current_modname()),
	config = {
		water = minetest.settings:get_bool("dynamic_liquid_water", true),
		river_water = minetest.settings:get_bool("dynamic_liquid_river_water", false),
		lava = minetest.settings:get_bool("dynamic_liquid_lava", true),
		water_probability = tonumber(minetest.settings:get("dynamic_liquid_water_flow_propability")) or 1,
		river_water_probability = tonumber(minetest.settings:get("dynamic_liquid_river_water_flow_propability")) or 1,
		lava_probability = tonumber(minetest.settings:get("dynamic_liquid_lava_flow_propability")) or 5,
		water_level = tonumber(minetest.get_mapgen_setting("water_level")) or 0,
		springs = minetest.settings:get_bool("dynamic_liquid_springs", true),
		flow_through = minetest.settings:get_bool("dynamic_liquid_flow_through", true),
		mapgen_prefill = minetest.settings:get_bool("dynamic_liquid_mapgen_prefill", true),
		disable_flow_above = tonumber(minetest.settings:get("dynamic_liquid_disable_flow_above")), -- this one can be nil
		displace_liquid = minetest.settings:get_bool("dynamic_liquid_displace_liquid", true),
		new_lava_cooling = minetest.settings:get_bool("dynamic_liquid_new_lava_cooling", true),
		falling_obsidian = minetest.settings:get_bool("dynamic_liquid_falling_obsidian", false),
	},
	registered_liquids = {}, -- used by the flow-through node abm
	registered_liquid_neighbors = {},
	mapgen_data = {}, -- shared by various mapgens
} -- global table to expose liquid_abm for other mods' usage

local function deep_copy(table_in)
	local table_out = {}
	for index, value in pairs(table_in) do
		if type(value) == "table" then
			table_out[index] = deep_copy(value)
		else
			table_out[index] = value
		end
	end
	return table_out
end
-- utility function used when making clay into springs
dynamic_liquid.duplicate_def = function (name)
	local old_def = minetest.registered_nodes[name]
	return deep_copy(old_def)
end

dofile(dynamic_liquid.path.."/cooling_lava.lua")
dofile(dynamic_liquid.path.."/dynamic_liquids.lua")
dofile(dynamic_liquid.path.."/flow_through.lua")
dofile(dynamic_liquid.path.."/springs.lua")
dofile(dynamic_liquid.path.."/mapgen_prefill.lua")

if minetest.global_exists("default") then
	dofile(dynamic_liquid.path.."/default.lua")
end

if minetest.global_exists("mcl_core") then
	dofile(dynamic_liquid.path.."/mineclone.lua")
end
