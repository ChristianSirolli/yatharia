function skills.stop(self)
	if not self.is_active then return false end
	self.is_active = false

	if not minetest.get_player_by_name(self.pl_name) then return false end

	skills.play_sound(self, self.sounds.stop, true)

	-- I don't know. MT is weird or maybe my code is just bugged:
	-- without this after, if the skills ends very quickly the
	-- spawner and the sound simply... don't stop.
	minetest.after(0, function()
		-- Stop sound
		if self.data._bgm then minetest.sound_stop(self.data._bgm) end

		-- Remove particles
		if self.data._particles then
			for i, spawner_id in pairs(self.data._particles) do
				minetest.delete_particlespawner(spawner_id)
			end
		end

		-- Remove hud
		if self.data._hud then
			for name, id in pairs(self.data._hud) do
				self.player:hud_remove(id)
			end
		end
	end)

	-- Restore sky
	if self.sky then
		local pl = self.player
		pl:set_sky(self.data._sky)
		self.data._sky = {}
	end

	-- Restore clouds
	if self.clouds then
		local pl = self.player
		pl:set_clouds(self.data._clouds)
		self.data._clouds = {}
	end

	-- Undo physics_override changes
	if self.physics then
		local reverse = {
			["multiply"] = "divide",
			["divide"] = "multiply",
			["add"] = "sub",
			["sub"] = "add",
		}
		local operation = reverse[self.physics.operation] -- multiply/divide/add/sub
		
		for property, value in pairs(self.physics) do
			if property ~= "operation" then
				_G["skills"][operation.."_physics"](self.pl_name, property, value)
			end
		end
	end

	self:on_stop()

	return true
end