skills = {
    path = minetest.get_modpath("skills")
}

dofile(skills.path .. "/src/utils.lua")
dofile(skills.path .. "/src/chatcmdbuilder.lua")

dofile(skills.path .. "/src/database.lua")

dofile(skills.path .. "/src/physics_manipulation.lua")

dofile(skills.path .. "/src/cooldown.lua")

dofile(skills.path .. "/src/cast.lua")
dofile(skills.path .. "/src/start.lua")
dofile(skills.path .. "/src/stop.lua")

dofile(skills.path .. "/src/attached_entities.lua")

dofile(skills.path .. "/src/core.lua")

dofile(skills.path .. "/src/commands.lua")