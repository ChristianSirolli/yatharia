if (minetest.settings:get_bool("clothing_enable_craft", true) == false) then
  if (minetest.settings:get_bool("clothing_register_clothes", true) == true) then
    
    dofile(clothing.path.."/colors_pictures.lua")
    dofile(clothing.path.."/clothes.lua")
    
    --clothing.basic_colors = nil;
    clothing.colors = nil;
    clothing.pictures = nil;
    minetest.log("warning", "Register of machines is disabled.")
  else
    minetest.log("warning", "Register of clothes and machines is disabled.")
  end
  return
else
  if (minetest.global_exists("appliances")==nil) then
    error("Cannot register clothes and machines because of missing mod appliances. Please install mod appliances or disable register of clothes and machines by disabling clothing_enable_craft in settings (aviable from Minetest main menu).")
	  return
  end
end

dofile(clothing.path.."/colors_pictures.lua")
dofile(clothing.path.."/craftitems.lua")
dofile(clothing.path.."/clothes.lua")
dofile(clothing.path.."/spinning_machine.lua")
dofile(clothing.path.."/loom.lua")
dofile(clothing.path.."/dirty_water.lua")
dofile(clothing.path.."/dye_machine.lua")
dofile(clothing.path.."/mannequin.lua")
dofile(clothing.path.."/sewing_table.lua")
dofile(clothing.path.."/crafting.lua")

--clothing.basic_colors = nil;
clothing.colors = nil;
clothing.pictures = nil;

