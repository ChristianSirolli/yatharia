pathogen = {
  pathogens = {},
  infections = {},
  fluids = {},
  path = minetest.get_modpath( "pathogen" )
}

dofile( pathogen.path .. "/options.lua" ) --WIP
dofile( pathogen.path .. "/recipes.lua")
dofile( pathogen.path .. "/api.lua" )
dofile( pathogen.path .. "/tools.lua" )
dofile( pathogen.path .. "/crafts.lua" )
dofile( pathogen.path .. "/nodes.lua" )
dofile( pathogen.path .. "/commands.lua" )
