

skeletons = {
  path = minetest.get_modpath('skeletons')
};

skeletons.have_bones = minetest.global_exists("bones") or minetest.global_exists("hades_bones");
skeletons.have_animal = minetest.global_exists("mobs_animal");
if (not skeletons.have_animal) then
  skeletons.have_animal = minetest.global_exists("hades_animals");
end

dofile(skeletons.path.."/functions.lua")

dofile(skeletons.path.."/player/player_skeleton.lua")
if skeletons.have_bones then
  dofile(skeletons.path.."/player/bones.lua")
end
dofile(skeletons.path.."/player/skulls.lua")

dofile(skeletons.path.."/villager/villager_skeleton.lua")

dofile(skeletons.path.."/mobs_animal/bunny_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/chicken_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/cow_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/kitten_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/panda_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/penguin_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/warthog_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/rat_skeleton.lua")
dofile(skeletons.path.."/mobs_animal/sheep_skeleton.lua")

dofile(skeletons.path.."/mob_horse/horse_skeleton.lua")

