--Register axes
local axe_types = {
  "axe_silver",
}

if not stripped_tree.ENABLE_CHISEL then
stripped_tree.register_axes("silver",axe_types)
end
