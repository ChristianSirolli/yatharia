bettertrees_api.register_tree('trees:clementine', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/clementine.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "clementine_trunk.png", "clementine_trunk_top.png", "clementine_trunk.png" },
    },
    leaves = {
        texture = 'clementine_leaves.png',
    },
    planks = {
        texture = 'clementine_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'clementine_twig.png',
    },
    sapling = {
        texture = 'clementine_sapling.png'
    }
})
