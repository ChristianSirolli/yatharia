bettertrees_api.register_tree('trees:lemon', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/lemon.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "lemon_trunk.png", "lemon_trunk_top.png", "lemon_trunk.png" },
    },
    leaves = {
        texture = 'lemon_leaves.png',
    },
    planks = {
        texture = 'lemon_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'lemon_twig.png',
    },
    sapling = {
        texture = 'lemon_sapling.png'
    }
})
