bettertrees_api.register_tree('trees:cacao', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/cacao.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "cacao_trunk.png", "cacao_trunk_top.png", "cacao_trunk.png" },
    },
    leaves = {
        texture = 'cacao_leaves.png',
    },
    planks = {
        texture = 'cacao_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'cacao_twig.png',
    },
    sapling = {
        texture = 'cacao_sapling.png'
    }
})
