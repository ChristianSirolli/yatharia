bettertrees_api.register_tree('trees:palm', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/palm.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "palm_trunk.png", "palm_trunk_top.png", "palm_trunk.png" },
    },
    leaves = {
        texture = 'palm_leaves.png',
    },
    planks = {
        texture = 'palm_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'palm_twig.png',
    },
    sapling = {
        texture = 'palm_sapling.png'
    }
})
