bettertrees_api.register_tree('trees:pomegranate', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/pomegranate.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "pomegranate_trunk.png", "pomegranate_trunk_top.png", "pomegranate_trunk.png" },
    },
    leaves = {
        texture = 'pomegranate_leaves.png',
    },
    planks = {
        texture = 'pomegranate_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'pomegranate_twig.png',
    },
    sapling = {
        texture = 'pomegranate_sapling.png'
    }
})
