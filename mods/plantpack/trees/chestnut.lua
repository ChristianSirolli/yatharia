bettertrees_api.register_tree('trees:chestnut', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/chestnut.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "chestnut_trunk.png", "chestnut_trunk_top.png", "chestnut_trunk.png" },
    },
    leaves = {
        texture = 'chestnut_leaves.png',
    },
    planks = {
        texture = 'chestnut_wood.png'
    },
    fruit = {
        description = 'Chestnut',
        texture = '',
        edible = false,
    },
    seed = {
        description = 'Cracked Chestnut',
        texture = '',
        edible = true,
        roastable = true,
    },
    sticks = {
        texture = 'chestnut_twig.png',
    },
    sapling = {
        texture = 'chestnut_sapling.png'
    }
})
