bettertrees_api = {
    path = minetest.get_modpath('bettertrees_api'),
    mods = {
        default = minetest.get_modpath('default'),
        armor = minetest.get_modpath('3d_armor'),
        stripped_tree = minetest.get_modpath('stripped_tree'),
        hunger_ng = minetest.get_modpath('hunger_ng')
    },
    registered_trees = {}
}

math.randomseed(os.time())

function bettertrees.uuid()
    local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function(c)
        local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
        return string.format('%x', v)
    end)
end

function bettertrees.seed_on_place(itemstack, placer, pointed_thing, sapling_name, minp_relative, maxp_relative,
                                   interval)
end

function bettertrees.sapling_on_place(itemstack, placer, pointed_thing, sapling_name, minp_relative, maxp_relative,
                                      interval)
end

--- Register a tree by defining the trunk, leaves, planks, fruit, seeds, and sticks
---@param name string
---@param def table
function bettertrees_api.register_tree(name, def)
    --[[
        Example usage:
        bettertrees_api.register_tree('oak_tree:oak', {
            description = 'Oak Tree',
            fruit_same_as_seed = true, --acorns are planted directly
            trunk = {
                tiles = {"oak_tree_top.png", "oak_tree_top.png", "oak_tree.png"},
            },
            leaves = {
                texture = '',
            },
            planks = {
                texture = 'oak_tree_wood.png'
            },
            fruit = {
                name = 'oak_tree:acorn',
                description = 'Acorn',
                groups = ''
            },
            sticks = {
                texture = 'oak_tree_stick.png',
            },
            sapling = {
                texture = 'oak_tree_sapling.png'
            }
        })        
    ]]
    -- Definitions
    local name = name or 'bettertrees_api:tree'
    local modname_i, _ = string.find(name, ':')
    local modname = string.sub(name, 0, modname_i - 1)
    local tree = {
        description = def.description or 'Fruit Tree',
        fruit_same_as_seed = def.fruit_same_as_seed or false,
        trunk = {
            description = def.trunk and def.trunk.description or 'Trunk',
            tiles = def.trunk and def.trunk.tiles or
                { "default_tree_top.png", "default_tree_top.png", "default_tree.png" },
            groups = {
                tree = 1,
                choppy = 2,
                oddly_breakable_by_hand = 0,
                flammable = 2,
                falling_node = 1
            },
            burntime = def.trunk.burntime or 30,
            sounds = def.trunk and def.trunk.sounds or (def.planks and def.planks.sounds or
                (def.sticks and def.sticks.sounds or
                    (bettertrees_api.mods.default and default.node_sound_wood_defaults() or nil)))
        },
        planks = {
            description = def.planks and def.planks.description or 'Wood Planks',
            texture = def.planks and def.planks.texture or 'default_wood.png',
            groups = {
                choppy = 2,
                oddly_breakable_by_hand = 2,
                flammable = 2,
                wood = 1
            },
            burntime = def.planks.burntime or 7,
            sounds = def.planks and def.planks.sounds or (def.trunk and def.trunk.sounds or
                (def.sticks and def.sticks.sounds or
                    (bettertrees_api.mods.default and default.node_sound_wood_defaults() or nil)))
        },
        leaves = {
            name = def.leaves and def.leaves.name or name .. '_leaves',
            description = def.leaves and def.leaves.description or 'Leaves',
            texture = def.leaves and def.leaves.texture or 'default_leaves.png',
            flowering_texture = def.leaves and def.leaves.flowering_texture or nil,
            burntime = def.leaves.burntime or 4,
            special_textures = def.leaves and def.leaves.special_textures or { "default_leaves_simple.png" },
            sounds = def.leaves and def.leaves.sounds or (def.sapling and def.sapling.sounds or
                (def.fruit and def.fruit.sounds or
                    (bettertrees_api.mods.default and default.node_sound_leaves_defaults() or nil))),
            groups = {
                snappy = 3,
                leafdecay = 3,
                flammable = 2,
                leaves = 1
            }
        },
        fruit = {
            name = def.fruit and def.fruit.name or name .. '_fruit',
            description = def.fruit and def.fruit.description or 'Fruit',
            groups = {
                fleshy = 3,
                dig_immediate = 3,
                flammable = 2,
                leafdecay = 3,
                leafdecay_drop = 1,
                food_apple = 1
            },
            edible = {
                raw = def.fruit and def.fruit.edible and def.fruit.edible.raw or true,
                cooked = def.fruit and def.fruit.edible and def.fruit.edible.cooked or true
            },
            sounds = def.fruit and def.fruit.sounds or (def.leaves and def.leaves.sounds or
                (def.sapling and def.sapling.sounds or
                    (bettertrees_api.mods.default and default.node_sound_leaves_defaults() or nil)))
        },
        seeds = {
            description = def.seeds and def.seeds.description or 'Seed',
            groups = {
                attached_node = 1,
                falling_node = 1,
                oddly_breakable_by_hand = 3,
                flammable = 2,
                dig_immediate = 3,
                snappy = 3,
                seed = 1
            },
            edible = {
                raw = def.fruit and def.fruit.edible and def.fruit.edible.raw or false,
                cooked = def.fruit and def.fruit.edible and def.fruit.edible.cooked or true
            },
            sounds = nil
        },
        sapling = {
            description = def.sapling and def.sapling.description or 'Sapling',
            texture = def.sapling and def.sapling.texture or 'default_sapling.png',
            groups = {
                snappy = 2,
                dig_immediate = 3,
                flammable = 2,
                attached_node = 1,
                sapling = 1
            },
            burntime = def.sapling.burntime or 5,
            sounds = def.sapling and def.sapling.sounds or (def.leaves and def.leaves.sounds or
                (def.fruit and def.fruit.sounds or
                    (bettertrees_api.mods.default and default.node_sound_leaves_defaults() or nil)))
        },
        sticks = {
            description = def.sticks and def.sticks.description or 'Stick',
            texture = def.sticks and def.sticks.texture or 'default_stick.png',
            groups = {
                stick = 1,
                falling_node = 1,
                oddly_breakable_by_hand = 3,
                flammable = 2,
                attached_node = 1,
                dig_immediate = 3,
                snappy = 3
            },
            burntime = def.trunk.burntime or 1,
            sounds = def.sticks and def.sticks.sounds or (def.planks and def.planks.sounds or
                (def.trunk and def.trunk.sounds or
                    (bettertrees_api.mods.default and default.node_sound_wood_defaults() or nil)))
        },
        biomes = {},
        products = {
            fences = def.products.fences or false,
            stairs = def.products.fences or false,
            armor = def.products.fences or false, -- 3d_armor support, https://www.timelessmyths.com/history/wooden-armor
            sword = def.products.fences or false,
            axe = def.products.fences or false,
            pickaxe = def.products.fences or false,
            hoe = def.products.fences or false,
            shovel = def.products.fences or false,
        }
    }
    bettertrees_api.registered_trees[name] = {
        def = tree,
        schematic = def.schematic,
    }
    -- Merge groups
    local types = { "trunk", "planks", "leaves", "fruit", "seeds", "sapling", "sticks" }
    for i, type in ipairs(types) do
        if def[type] and def[type].groups then
            for k, v in pairs(def[type].groups) do
                tree[type].groups[k] = v
            end
        end
    end
    -- Register nodes
    minetest.register_node(':' .. name .. '_trunk', {
        description = tree.description .. ' ' .. tree.trunk.description,
        tiles = tree.trunk.tiles,
        paramtype2 = "facedir",
        is_ground_content = false,
        groups = tree.trunk.groups,
        sounds = tree.trunk.sounds,
        on_place = minetest.rotate_node,
        on_punch = function(pos, node, puncher, pointed_thing)
            local wielded_item = puncher:get_wielded_item();
            if wielded_item:is_empty() then
                -- take a little damage
                local hp = puncher:get_hp()
                puncher:set_hp(hp - 0.1, 'node_damage')
            end
            minetest.node_punch(pos, node, puncher, pointed_thing)
        end
    })
    if bettertrees_api.mods.stripped_tree then
        stripped_tree.register_trunk(modname, name .. '_trunk')
    end
    minetest.register_node(':' .. name .. '_wood', {
        description = tree.description .. ' ' .. tree.planks.description,
        paramtype2 = "facedir",
        place_param2 = 0,
        tiles = { tree.planks.tiles },
        is_ground_content = false,
        groups = tree.planks.groups,
        sounds = tree.planks.sounds
    })
    if def.seeds then
        minetest.register_node(":" .. name .. '_seed', {
            description = tree.description .. ' ' .. tree.seeds.description,
            drawtype = 'mesh',
            paramtype = 'light',
            paramtype2 = 'none',
            selection_box = {
                type = 'fixed',
                fixed = { -8 / 16, -8 / 16, -8 / 16, 8 / 16, -7 / 16, 8 / 16 }
            },
            mesh = 'extrusion_mesh_16.obj',
            tiles = { tree.seeds.texture },
            use_texture_alpha = 'clip',
            inventory_image = tree.seeds.texture,
            floodable = true,
            walkable = false,
            sunlight_propagates = true,
            buildable_to = true,
            is_ground_content = false,
            groups = tree.seeds.groups,
            sounds = tree.seeds.sounds,
            on_place = function(itemstack, placer, pointed_thing)
                itemstack = default.sapling_on_place(itemstack, placer, pointed_thing, name .. '_sapling',
                    -- minp, maxp to be checked, relative to sapling pos
                    -- minp_relative.y = 1 because sapling pos has been checked
                    {
                        x = -2,
                        y = 1,
                        z = -2
                    }, {
                        x = 2,
                        y = 14,
                        z = 2
                    }, -- maximum interval of interior volume check
                    4)

                return itemstack
            end
        })
    end
    minetest.register_node(':' .. name .. '_sapling', {
        description = tree.description .. ' ' .. tree.sapling.description,
        drawtype = "plantlike",
        tiles = { tree.sapling.texture },
        inventory_image = tree.sapling.texture,
        wield_image = tree.sapling.texture,
        paramtype = "light",
        sunlight_propagates = true,
        walkable = false,
        on_timer = grow_sapling,
        selection_box = {
            type = "fixed",
            fixed = { -4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16 }
        },
        groups = tree.sapling.groups,
        sounds = tree.sapling.sounds,
        on_construct = function(pos)
            minetest.get_node_timer(pos):start(math.random(300, 1500))
        end,
        on_place = function(itemstack, placer, pointed_thing)
            itemstack = default.sapling_on_place(itemstack, placer, pointed_thing, name .. '_sapling',
                -- minp, maxp to be checked, relative to sapling pos
                -- minp_relative.y = 1 because sapling pos has been checked
                {
                    x = -2,
                    y = 1,
                    z = -2
                }, {
                    x = 2,
                    y = 14,
                    z = 2
                }, -- maximum interval of interior volume check
                4)

            return itemstack
        end
    })
    minetest.register_node(':' .. name .. '_leaves', {
        description = tree.description .. ' ' .. tree.leaves.description,
        drawtype = "allfaces_optional",
        waving = 2,
        walkable = false,
        climbable = true,
        move_resistance = 3,
        tiles = { tree.leaves.texture },
        special_tiles = tree.leaves.special_textures,
        paramtype = "light",
        is_ground_content = false,
        groups = tree.leaves.groups,
        drop = name .. '_leaves',
        sounds = tree.leaves.sounds,

        -- after_place_node = after_place_leaves,
        -- Make leaves fall when placed
        after_place_node = function(pos, placer, itemstack, pointed_thing)
            if placer and placer:is_player() then
                local node = minetest.get_node(pos)
                node.param2 = 1
                minetest.set_node(pos, node)
                minetest.spawn_falling_node(pos)
            end
            return after_place_leaves(pos, placer, itemstack, pointed_thing)
        end,
        -- Adapted from Tenplus1's regrow mod, licensed under MIT
        after_dig_node = function(pos, oldnode, oldmetadata, digger)
            -- if node has been placed by player then do not regrow
            if oldnode.param2 > 0 then
                return
            end
            -- replace leaf with regrowth node, set leaf name
            spawn_hidden(pos, node_name)
        end,
        on_construct = function(pos)
            local time = math.random(60 * 20, 60 * 30)
            minetest.get_node_timer(pos):start(time)
        end,
        -- when timer reached, check which leaf to remove
        on_timer = function(pos, elapsed)
            if math.random(200) == 1 then
                local pos2 = table.copy(pos)
                pos2.y = pos2.y - 1
                local check_node = minetest.get_node(pos2)
                if check_node.name == 'air' then
                    break_leaf(pos, node_name, true)
                    return false
                end
            end
            return true
        end
    })
    minetest.register_node(":" .. name .. '_leaves_mark', {
        description = tree.description .. ' ' .. tree.fruit.description .. ' Mark',
        inventory_image = "default_apple.png^default_invisible_node_overlay.png",
        wield_image = "default_apple.png^default_invisible_node_overlay.png",
        drawtype = "airlike",
        paramtype = "light",
        sunlight_propagates = true,
        walkable = false,
        pointable = false,
        diggable = false,
        buildable_to = true,
        drop = "",
        groups = {
            not_in_creative_inventory = 1,
            dig_immediate = 3
        },
        on_timer = function(pos, elapsed)
            if not minetest.find_node_near(pos, 1, name .. '_leaves') then
                minetest.remove_node(pos)
            elseif minetest.get_node_light(pos) < 11 then
                minetest.get_node_timer(pos):start(200)
            else
                minetest.set_node(pos, {
                    name = name .. '_leaves'
                })
            end
        end
    })
    if def.fruit then
        minetest.register_node(":" .. tree.fruit.name, {
            description = tree.description .. ' ' .. tree.fruit.description,
            drawtype = "plantlike",
            tiles = { tree.fruit.texture },
            inventory_image = tree.fruit.texture,
            paramtype = "light",
            sunlight_propagates = true,
            walkable = false,
            is_ground_content = false,
            selection_box = {
                type = "fixed",
                fixed = { -3 / 16, -7 / 16, -3 / 16, 3 / 16, 4 / 16, 3 / 16 }
            },
            groups = tree.fruit.groups,
            on_use = minetest.item_eat(2, tree.fruit_same_as_seed == false and name .. '_seed' or nil),
            sounds = tree.fruit.sounds,
            after_place_node = function(pos, placer, itemstack)
                minetest.set_node(pos, {
                    name = tree.fruit.name,
                    param2 = 1
                })
            end,

            after_dig_node = function(pos, oldnode, oldmetadata, digger)
                if oldnode.param2 == 0 then
                    minetest.set_node(pos, {
                        name = tree.fruit.name .. '_mark'
                    })
                    minetest.get_node_timer(pos):start(math.random(300, 1500))
                end
            end
        })
        if tree.fruit.edible.cooked.enabled then
            minetest.register_node(":" .. tree.fruit.name .. '_cooked', {
                description = tree.description .. ' ' .. tree.fruit.description,
                drawtype = "plantlike",
                tiles = { tree.fruit.texture },
                inventory_image = tree.fruit.texture,
                paramtype = "light",
                sunlight_propagates = true,
                walkable = false,
                is_ground_content = false,
                selection_box = {
                    type = "fixed",
                    fixed = { -3 / 16, -7 / 16, -3 / 16, 3 / 16, 4 / 16, 3 / 16 }
                },
                groups = tree.fruit.groups,
                on_use = minetest.item_eat(tree.fruit.edible.cooked.satiation,
                    tree.fruit_same_as_seed == false and name .. '_seed' or nil),
                sounds = tree.fruit.sounds,
                after_place_node = function(pos, placer, itemstack)
                    minetest.set_node(pos, {
                        name = tree.fruit.name .. '_cooked',
                        param2 = 1
                    })
                end
            })
        end
        minetest.register_node(":" .. tree.fruit.name .. '_mark', {
            description = tree.description .. ' ' .. tree.fruit.description .. ' Mark',
            inventory_image = tree.fruit.texture .. "^default_invisible_node_overlay.png",
            wield_image = tree.fruit.texture .. "^default_invisible_node_overlay.png",
            drawtype = "airlike",
            paramtype = "light",
            sunlight_propagates = true,
            walkable = false,
            pointable = false,
            diggable = false,
            buildable_to = true,
            drop = "",
            groups = {
                not_in_creative_inventory = 1,
                dig_immediate = 3
            },
            on_timer = function(pos, elapsed)
                if not minetest.find_node_near(pos, 1, name .. "_leaves") then
                    minetest.remove_node(pos)
                elseif minetest.get_node_light(pos) < 11 then
                    minetest.get_node_timer(pos):start(200)
                else
                    minetest.set_node(pos, {
                        name = tree.fruit.name
                    })
                end
            end
        })
        if bettertrees_api.mods.hunger_ng then
            if tree.fruit.edible.raw.enabled then
                hunger_ng.add_hunger_data(tree.fruit.name, {
                    satiates = tree.fruit.edible.raw.satiation
                })
            end
            if tree.fruit.edible.cooked.enabled then
                hunger_ng.add_hunger_data(tree.fruit.name .. '_cooked', {
                    satiates = tree.fruit.edible.cooked.satiation
                })
            end
            if tree.seeds.edible.raw.enabled then
                hunger_ng.add_hunger_data(tree.seeds.name, {
                    satiates = tree.seeds.edible.raw.satiation
                })
            end
            if tree.seeds.edible.cooked.enabled then
                hunger_ng.add_hunger_data(tree.seeds.name .. '_cooked', {
                    satiates = tree.seeds.edible.cooked.satiation
                })
            end
        end
    end
    minetest.register_node(":" .. name .. '_stick', {
        description = tree.description .. ' ' .. tree.sticks.description,
        drawtype = 'mesh',
        paramtype = 'light',
        paramtype2 = 'none',
        selection_box = {
            type = 'fixed',
            fixed = { -8 / 16, -8 / 16, -8 / 16, 8 / 16, -7 / 16, 8 / 16 }
        },
        mesh = 'extrusion_mesh_16.obj',
        tiles = { tree.sticks.texture },
        use_texture_alpha = 'clip',
        inventory_image = tree.sticks.texture,
        floodable = true,
        walkable = false,
        sunlight_propagates = true,
        buildable_to = true,
        is_ground_content = false,
        groups = tree.sticks.groups,
        sounds = tree.sticks.sounds
    })
    if def.schematics then

    end
    minetest.register_craft({
        type = "fuel",
        recipe = name .. '_trunk',
        burntime = tree.trunk.burntime,
    })
    minetest.register_craft({
        type = "fuel",
        recipe = name .. '_wood',
        burntime = tree.planks.burntime,
    })
    minetest.register_craft({
        type = "fuel",
        recipe = name .. '_sapling',
        burntime = tree.sapling.burntime,
    })
    minetest.register_craft({
        type = "fuel",
        recipe = name .. '_leaves',
        burntime = tree.leaves.burntime,
    })
    minetest.register_craft({
        type = "fuel",
        recipe = name .. '_stick',
        burntime = tree.sticks.burntime,
    })
    if minetest.global_exists("armor") and tree.armor then
        armor:register_armor(name .. "_helmet", {
            description = tree.description .. " Chestplate",
            inventory_image = "mod_name_inv_chestplate_leather.png",
            texture = "mod_name_leather_chestplate.png",
            preview = "mod_name_leather_chestplate_preview.png",
            groups = { armor_torso = 1, armor_heal = 0, armor_use = 2000, flammable = 1 },
            armor_groups = { fleshy = 10 },
            damage_groups = { cracky = 3, snappy = 2, choppy = 3, crumbly = 2, level = 1 }
        })
        armor:register_armor(name .. "_chestplate", {
            description = tree.description .. " Chestplate",
            inventory_image = "mod_name_inv_chestplate_leather.png",
            texture = "mod_name_leather_chestplate.png",
            preview = "mod_name_leather_chestplate_preview.png",
            groups = { armor_torso = 1, armor_heal = 0, armor_use = 2000, flammable = 1 },
            armor_groups = { fleshy = 10 },
            damage_groups = { cracky = 3, snappy = 2, choppy = 3, crumbly = 2, level = 1 }
        })
        armor:register_armor(name .. "_leggings", {
            description = tree.description .. " Chestplate",
            inventory_image = "mod_name_inv_chestplate_leather.png",
            texture = "mod_name_leather_chestplate.png",
            preview = "mod_name_leather_chestplate_preview.png",
            groups = { armor_torso = 1, armor_heal = 0, armor_use = 2000, flammable = 1 },
            armor_groups = { fleshy = 10 },
            damage_groups = { cracky = 3, snappy = 2, choppy = 3, crumbly = 2, level = 1 }
        })
        armor:register_armor(name .. "_boots", {
            description = tree.description .. " Chestplate",
            inventory_image = "mod_name_inv_chestplate_leather.png",
            texture = "mod_name_leather_chestplate.png",
            preview = "mod_name_leather_chestplate_preview.png",
            groups = { armor_torso = 1, armor_heal = 0, armor_use = 2000, flammable = 1 },
            armor_groups = { fleshy = 10 },
            damage_groups = { cracky = 3, snappy = 2, choppy = 3, crumbly = 2, level = 1 }
        })
        if minetest.global_exists('shields') then
            local disable_sounds = minetest.settings:get_bool("shields_disable_sounds")
            local function play_sound_effect(player, name)
                if not disable_sounds and player then
                    local pos = player:get_pos()
                    if pos then
                        minetest.sound_play(name, {
                            pos = pos,
                            max_hear_distance = 10,
                            gain = 0.5,
                        })
                    end
                end
            end

            armor:register_armor(name .. "_shield", {
                description = tree.description .. " Chestplate",
                inventory_image = "shields_inv_shield_wood.png",
                groups = { armor_shield = 1, armor_heal = 0, armor_use = 2000, flammable = 1 },
                armor_groups = { fleshy = 5 },
                damage_groups = { cracky = 3, snappy = 2, choppy = 3, crumbly = 2, level = 1 },
                reciprocate_damage = true,
                on_damage = function(player, index, stack)
                    play_sound_effect(player, "default_wood_footstep")
                end,
                on_destroy = function(player, index, stack)
                    play_sound_effect(player, "default_wood_footstep")
                end,
            })

            minetest.register_craft({
                type = "fuel",
                recipe = name .. "_shield",
                burntime = 8,
            })
        end
    end
end
