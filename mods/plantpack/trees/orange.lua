bettertrees_api.register_tree('trees:orange', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/orange.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "orange_trunk.png", "orange_trunk_top.png", "orange_trunk.png" },
    },
    leaves = {
        texture = 'orange_leaves.png',
    },
    planks = {
        texture = 'orange_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'orange_twig.png',
    },
    sapling = {
        texture = 'orange_sapling.png'
    }
})
