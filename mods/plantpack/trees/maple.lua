bettertrees_api.register_tree('trees:maple', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/maple.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "maple_trunk.png", "maple_trunk_top.png", "maple_trunk.png" },
    },
    leaves = {
        texture = 'maple_leaves.png',
    },
    planks = {
        texture = 'maple_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'maple_twig.png',
    },
    sapling = {
        texture = 'maple_sapling.png'
    }
})
