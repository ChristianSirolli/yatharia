bettertrees_api.register_tree('trees:holly', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/holly.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "holly_trunk.png", "holly_trunk_top.png", "holly_trunk.png" },
    },
    leaves = {
        texture = 'holly_leaves.png',
    },
    planks = {
        texture = 'holly_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'holly_twig.png',
    },
    sapling = {
        texture = 'holly_sapling.png'
    }
})
