bettertrees_api.register_tree('trees:aspen', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/aspen.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "aspen_trunk.png", "aspen_trunk_top.png", "aspen_trunk.png" },
    },
    leaves = {
        texture = 'aspen_leaves.png',
    },
    planks = {
        texture = 'aspen_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'aspen_twig.png',
    },
    sapling = {
        texture = 'aspen_sapling.png'
    }
})
