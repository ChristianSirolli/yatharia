bettertrees_api.register_tree('trees:baldcypress', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/baldcypress.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "baldcypress_trunk.png", "baldcypress_trunk_top.png", "baldcypress_trunk.png" },
    },
    leaves = {
        texture = 'baldcypress_leaves.png',
    },
    planks = {
        texture = 'baldcypress_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'baldcypress_twig.png',
    },
    sapling = {
        texture = 'baldcypress_sapling.png'
    }
})
