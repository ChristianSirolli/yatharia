bettertrees_api.register_tree('trees:willow', {
    description = 'Willow Tree',
    schematics = { trees.path .. "/schematics/willow.mts" },
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "willow_trunk.png", "willow_trunk_top.png", "willow_trunk.png" },
    },
    leaves = {
        texture = 'willow_leaves.png',
    },
    planks = {
        texture = 'willow_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'willow_twig.png',
    },
    sapling = {
        texture = 'willow_sapling.png'
    }
})
