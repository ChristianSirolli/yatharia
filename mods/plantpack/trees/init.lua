trees = {
    path = minetest.get_modpath('trees'),
    trees = {
        'acacia', -- new
        'apple',
        'aspen',
        'baldcypress',
        'banana', -- new
        'birch',
        'cacao',
        'cherry',
        'chestnut',
        'clementine',
        'coconut_palm', -- new
        'holly',
        'jacaranda',
        'larch',
        'lemon',
        'mahogany',
        'maple',
        'nightshade',
        'oak',
        'olive', -- new
        'orange',
        'palm',
        'pine', -- new
        'pomegranate',
        'redwood', -- new
        'rubber', -- new
        'sakura', -- new
        'scorched', -- new, lacks a lot
        'sequoia',
        'willow'
    },
}

dofile(trees.path..'/bettertrees_api.lua')

for index, tree in ipairs(trees.trees) do
    dofile(trees.path..'/'..tree..'.lua')
end

-- Notes:
-- Bamboo is not a tree, it is an evergreen perennial flowering plants.
-- Cacti are not trees, they are succulent perennial plants.
-- As such, these should not be registered as trees.
-- 
-- "Jungle trees" should not exist, as this is actually racist towards trees.
-- Tropicle rain forests typically support multilayered canopies composed of
--   many different types of trees.
-- https://sciencing.com/types-trees-grow-jungle-5031331.html