bettertrees_api.register_tree('trees:cherry', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/cherry.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "cherry_trunk.png", "cherry_trunk_top.png", "cherry_trunk.png" },
    },
    leaves = {
        texture = 'cherry_leaves.png',
    },
    planks = {
        texture = 'cherry_wood.png'
    },
    fruit = {
        description = 'Cherry',
        texture = 'cherry.png',
        edible = true,
    },
    seeds = {
        description = 'Cherry Pit',
        name = 'trees:cherry_pit',
        edible = false,
    },
    sticks = {
        texture = 'cherry_twig.png',
    },
    sapling = {
        texture = 'cherry_sapling.png'
    }
})
