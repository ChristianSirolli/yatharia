bettertrees_api.register_tree('trees:plum', {
    description = 'Willow Tree',
    schematics = { trees.path .. "/schematics/plum.mts" },
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "plum_trunk.png", "plum_trunk_top.png", "plum_trunk.png" },
    },
    leaves = {
        texture = 'plum_leaves.png',
    },
    planks = {
        texture = 'plum_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'default_stick.png',
    },
    sapling = {
        texture = 'plum_sapling.png'
    }
})
