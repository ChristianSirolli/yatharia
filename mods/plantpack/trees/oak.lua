bettertrees_api.register_tree('trees:oak', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/oak.mts",
    fruit_same_as_seed = true,
    trunk = {
        tiles = { "oak_trunk.png", "oak_trunk_top.png", "oak_trunk.png" },
    },
    leaves = {
        texture = 'oak_leaves.png',
    },
    planks = {
        texture = 'oak_wood.png'
    },
    fruit = {
        name = 'trees:acorn',
        description = 'Acorn',
        groups = ''
    },
    sticks = {
        texture = 'oak_twig.png',
    },
    sapling = {
        texture = 'oak_sapling.png'
    }
})
