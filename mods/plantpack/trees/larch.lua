bettertrees_api.register_tree('trees:larch', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/larch.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "larch_trunk.png", "larch_trunk_top.png", "larch_trunk.png" },
    },
    leaves = {
        texture = 'larch_leaves.png',
    },
    planks = {
        texture = 'larch_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'larch_twig.png',
    },
    sapling = {
        texture = 'larch_sapling.png'
    }
})
