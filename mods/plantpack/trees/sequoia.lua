bettertrees_api.register_tree('trees:sequoia', {
    description = 'Sequoia Tree',
    schematics = { trees.path .. "/schematics/sequoia_01.mts", trees.path .. "/schematics/sequoia_02.mts",
        trees.path .. "/schematics/sequoia_03.mts" },
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "sequoia_trunk.png", "sequoia_trunk_top.png", "sequoia_trunk.png" },
        burntime = 30,
    },
    leaves = {
        texture = 'sequoia_leaves.png',
    },
    planks = {
        texture = 'sequoia_wood.png',
        burntime = 7,
    },
    -- fruit = {
    --     name = 'trees:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'default_stick.png',
    },
    sapling = {
        texture = 'sequoia_sapling.png'
    },
    biomes = {
        place_on = {"redw:dirt", "redw:dirt_with_grass"},
        biomes = {'redwood_forest'}
    },
    products = {
        fences = true, -- fences and gates
        stairs = true, -- stairs and slabs
    }
})
