bettertrees_api.register_tree('trees:mahogany', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/mahogany.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "mahogany_trunk.png", "mahogany_trunk_top.png", "mahogany_trunk.png" },
    },
    leaves = {
        texture = 'mahogany_leaves.png',
    },
    planks = {
        texture = 'mahogany_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'mahogany_twig.png',
    },
    sapling = {
        texture = 'mahogany_sapling.png'
    }
})
