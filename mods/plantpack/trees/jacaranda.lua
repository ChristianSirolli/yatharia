bettertrees_api.register_tree('trees:jacaranda', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/jacaranda.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "jacaranda_trunk.png", "jacaranda_trunk_top.png", "jacaranda_trunk.png" },
    },
    leaves = {
        texture = 'jacaranda_leaves.png',
    },
    planks = {
        texture = 'jacaranda_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'jacaranda_twig.png',
    },
    sapling = {
        texture = 'jacaranda_sapling.png'
    }
})
