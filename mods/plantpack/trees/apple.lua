bettertrees_api.register_tree('trees:apple', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/apple.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "apple_trunk.png", "apple_trunk_top.png", "apple_trunk.png" },
    },
    leaves = {
        texture = 'default_leaves.png',
    },
    planks = {
        texture = 'default_wood.png'
    },
    fruit = {
        description = 'Apple',
        edible = true,
        replacement = 'trees:apple_core',
        replacement_num = 1
    },
    seeds = {
        texture = 'apple_seeds.png',
    },
    sticks = {
        texture = 'default_stick.png',
    },
    sapling = {
        texture = 'default_sapling.png'
    }
})

minetest.register_craft({
    output = 'trees:apple_seed 9',
    recipe = {{'trees:apple_core'}}
})