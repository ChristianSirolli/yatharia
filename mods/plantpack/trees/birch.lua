bettertrees_api.register_tree('trees:birch', {
    description = 'Willow Tree',
    schematic = trees.path.."/schematics/birch.mts",
    -- fruit_same_as_seed = true,
    trunk = {
        tiles = { "birch_trunk.png", "birch_trunk_top.png", "birch_trunk.png" },
    },
    leaves = {
        texture = 'birch_leaves.png',
    },
    planks = {
        texture = 'birch_wood.png'
    },
    -- fruit = {
    --     name = 'oak_tree:acorn',
    --     description = 'Acorn',
    --     groups = ''
    -- },
    sticks = {
        texture = 'birch_twig.png',
    },
    sapling = {
        texture = 'birch_sapling.png'
    }
})
