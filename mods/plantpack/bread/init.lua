-- Baking Bread by Marc/Lahusen (C) 2022


bread = {
    path = minetest.get_modpath("bread")
}

dofile(bread.path .. "/pita.lua")
dofile(bread.path .. "/wholegraincarrotbread.lua")
