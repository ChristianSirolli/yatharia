dandelions = {
    path = minetest.get_modpath('dandelions'),
    mods = {
        default = minetest.global_exists('default'),
        flowers = minetest.global_exists('flowers'),
        wieldview = minetest.global_exists('wieldview'),
        cucina_vegana = minetest.global_exists('cucina_vegana')
    },
    flowers = {
        yellow = 'flowers:dandelion_yellow',
        white = 'flowers:dandelion_white',
    }
}

-- Replicate dandelions from the flowers mod if it is not present
if not dandelions.mods.flowers then
    dandelions.flowers.yellow = 'dandelions:dandelion_yellow'
    dandelions.flowers.white = 'dandelions:dandelion_white'
    
    -- Aliases
    minetest.register_alias(dandelions.flowers.white, "flowers:dandelion_white")
    minetest.register_alias(dandelions.flowers.white, "flowers:flower_dandelion_white")
    minetest.register_alias(dandelions.flowers.yellow, "flowers:dandelion_yellow")
    minetest.register_alias(dandelions.flowers.yellow, "flowers:flower_dandelion_yellow")
    minetest.register_alias(dandelions.flowers.yellow, "mcl_flowers:dandelion")

    -- Node definitions
    minetest.register_node(dandelions.flowers.white, {
        description = 'White Dandelion',
        drawtype = "plantlike",
        waving = 1,
        tiles = {"flowers_dandelion_white.png"},
        inventory_image = "flowers_dandelion_white.png",
        wield_image = "flowers_dandelion_white.png",
        sunlight_propagates = true,
        paramtype = "light",
        walkable = false,
        buildable_to = true,
        groups = {
            snappy = 3,
            flower = 1,
            flora = 1,
            attached_node = 1,
            color_white = 1,
            flammable = 1,
            dig_immediate = 3
        },
        sounds = dandelions.mods.default and default.node_sound_leaves_defaults() or nil,
        selection_box = {
            type = "fixed",
            fixed = {-5 / 16, -0.5, -5 / 16, 5 / 16, -2 / 16, 5 / 16}
        }
    })

    minetest.register_node(dandelions.flowers.yellow, {
        description = 'Yellow Dandelion',
        drawtype = "plantlike",
        waving = 1,
        tiles = {"flowers_dandelion_yellow.png"},
        inventory_image = "flowers_dandelion_yellow.png",
        wield_image = "flowers_dandelion_yellow.png",
        sunlight_propagates = true,
        paramtype = "light",
        walkable = false,
        buildable_to = true,
        groups = {
            snappy = 3,
            flower = 1,
            flora = 1,
            attached_node = 1,
            color_yellow = 1,
            flammable = 1,
            dig_immediate = 3
        },
        sounds = dandelions.mods.default and default.node_sound_leaves_defaults() or nil,
        selection_box = {
            type = "fixed",
            fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, -2 / 16, 4 / 16}
        }
    })

    -- Mapgen
    -- BUG: won't spawn if default is not present, since "default:dirt_with_grass" and biomes "grassland" and "deciduous_forest" are not registered otherwise
    minetest.register_decoration({
        name = dandelions.flowers.white,
        deco_type = "simple",
        place_on = {"default:dirt_with_grass"},
        sidelen = 16,
        noise_params = {
            offset = -0.02,
            scale = 0.04,
            spread = {x = 200, y = 200, z = 200},
            seed = 73133,
            octaves = 3,
            persist = 0.6
        },
        biomes = {"grassland", "deciduous_forest"},
        y_max = 31000,
        y_min = 1,
        decoration = dandelions.flowers.white,
    })

    minetest.register_decoration({
        name = dandelions.flowers.yellow,
        deco_type = "simple",
        place_on = {"default:dirt_with_grass"},
        sidelen = 16,
        noise_params = {
            offset = -0.02,
            scale = 0.04,
            spread = {x = 200, y = 200, z = 200},
            seed = 1220999,
            octaves = 3,
            persist = 0.6
        },
        biomes = {"grassland", "deciduous_forest"},
        y_max = 31000,
        y_min = 1,
        decoration = dandelions.flowers.yellow,
    })

    -- Flower spread ABM
    minetest.register_abm({
        label = "Flower spread",
        nodenames = {"group:flora"},
        interval = 13,
        chance = 300,
        action = function(...)
            pos.y = pos.y - 1
            local under = minetest.get_node(pos)
            pos.y = pos.y + 1
            -- Replace flora with dry shrub in desert sand and silver sand,
            -- as this is the only way to generate them.
            -- However, preserve grasses in sand dune biomes.
            if minetest.get_item_group(under.name, "sand") == 1 and
                    under.name ~= "default:sand" and dandelions.mods.default then
                minetest.set_node(pos, {name = "default:dry_shrub"})
                return
            end
        
            if minetest.get_item_group(under.name, "soil") == 0 then
                return
            end
        
            local light = minetest.get_node_light(pos)
            if not light or light < 13 then
                return
            end
        
            local pos0 = vector.subtract(pos, 4)
            local pos1 = vector.add(pos, 4)
            -- Testing shows that a threshold of 3 results in an appropriate maximum
            -- density of approximately 7 flora per 9x9 area.
            if #minetest.find_nodes_in_area(pos0, pos1, "group:flora") > 3 then
                return
            end
        
            local soils = minetest.find_nodes_in_area_under_air(
                pos0, pos1, "group:soil")
            local num_soils = #soils
            if num_soils >= 1 then
                for si = 1, math.min(3, num_soils) do
                    local soil = soils[math.random(num_soils)]
                    local soil_name = minetest.get_node(soil).name
                    local soil_above = {x = soil.x, y = soil.y + 1, z = soil.z}
                    light = minetest.get_node_light(soil_above)
                    if light and light >= 13 and
                            -- Only spread to same surface node
                            soil_name == under.name and
                            -- Desert sand is in the soil group
                            soil_name ~= "default:desert_sand" then
                        minetest.set_node(soil_above, {name = node.name})
                    end
                end
            end
        end,
    })

    -- Wieldview support
    -- wieldview.transform[dandelions.flowers.white]="R270"
    -- wieldview.transform[dandelions.flowers.yellow]="R270"
else
    minetest.override_item("flowers:dandelion_yellow",{groups = {dig_immediate = 3}})
    minetest.override_item("flowers:dandelion_white",{groups = {dig_immediate = 3}})
end

if not dandelions.mods.cucina_vegana then
    minetest.register_alias('dandelions:dandelion_honey', "cucina_vegana:dandelion_honey")
    minetest.register_alias("dandelions:dandelion_suds", "cucina_vegana:dandelion_suds")
    minetest.register_alias("dandelions:dandelion_suds_cooking", "cucina_vegana:dandelion_suds_cooking")

    minetest.register_craftitem("dandelions:dandelion_honey", {
        description = S("Dandelion Honey"),
        inventory_image = "cucina_vegana_dandelion_honey.png",
        groups = {flammable = 1, food = 1, food_honey = 1, food_sugar = 1, eatable = 1},
        on_use = minetest.item_eat(2),
    })
    
    minetest.register_craftitem("dandelions:dandelion_suds", {
        description = S("Dandelion Suds"),
        inventory_image = "cucina_vegana_dandelion_suds.png",
        groups = {sud = 1},
    })
    
    minetest.register_craftitem("dandelions:dandelion_suds_cooking", {
        description = S("Dandelion Suds (cooking)"),
        inventory_image = "cucina_vegana_dandelion_suds_cooking.png",
        groups = {sud = 1},
    })

    minetest.register_craft({
        output = "dandelions:dandelion_suds",
        recipe = {	{"flowers:dandelion_yellow", "flowers:dandelion_yellow", "flowers:dandelion_yellow"},
                    {"flowers:dandelion_yellow", "flowers:dandelion_yellow", "flowers:dandelion_yellow"},
                    {"", "bucket:bucket_water", ""}
                }
    })
    
    minetest.register_craft({
        output = "dandelions:dandelion_suds",
        recipe = {	{"flowers:dandelion_yellow", "flowers:dandelion_yellow", "flowers:dandelion_yellow"},
                    {"flowers:dandelion_yellow", "flowers:dandelion_yellow", "flowers:dandelion_yellow"},
                    {"", "bucket:bucket_river_water", ""}
                }
    })
    minetest.register_craft({
        output = "dandelions:dandelion_honey",
        recipe = {	{"dandelions:dandelion_suds_cooking", "", ""},
                    {"group:wool", "", ""},
                    {"vessels:glass_bottle", "", ""}
                },
        replacements = {
                {"dandelions:dandelion_suds_cooking", "bucket:bucket_empty"},
                {"group:wool", "farming:cotton 2"}
                       }
    })
    minetest.register_craft({
        type = "cooking",
        cooktime = 20,
        output = "cucina_vegana:dandelion_suds_cooking",
        recipe = "cucina_vegana:dandelion_suds"
    })
end