betterpm = {
  path = minetest.get_modpath("betterpm")
}

local S = minetest.get_translator("betterpm")
local storage = minetest.get_mod_storage()
local reply_to = {}

dofile(betterpm.path .. "/settings.lua")
dofile(betterpm.path .. "/chatcmdbuilder.lua")

local function formatPrefix(prefix, sender, target)
  local res, _ = prefix:gsub("{sender}", sender)
  res, _ = res:gsub("{target}", target)

  return res
end

local function sendMessage(sender, target, message)
  reply_to[sender] = target
  reply_to[target] = sender

  if not minetest.check_player_privs(sender, { shout=true }) and betterpm.requires_shout then
    minetest.chat_send_player(sender,
      minetest.colorize("#ff0000", S("You don't have the privilege to send private messages")))
    return
  end

  local ignored = minetest.deserialize(storage:get_string(target)) or {}
  if ignored[sender] or ignored[":all"] then
    minetest.chat_send_player(sender,
      minetest.colorize("#ff0000", S("You can't send messages to @1", target)))
    return
  end

  if minetest.get_player_by_name(target) then
    minetest.chat_send_player(target,
      minetest.colorize(betterpm.targetPrefixColor, formatPrefix(betterpm.targetPrefix, sender, target) ..
      minetest.colorize(betterpm.targetMsgColor, message)))
    minetest.chat_send_player(sender,
      minetest.colorize(betterpm.senderPrefixColor, formatPrefix(betterpm.senderPrefix, sender, target) ..
      minetest.colorize(betterpm.senderMsgColor, message)))
  else
    minetest.chat_send_player(sender, minetest.colorize("#ff0000", S("@1 is not online", target)))
  end
end

ChatCmdBuilder.new("msg", function(cmd)
  cmd:sub(":target :message:text", function (sender, target, message)
    sendMessage(sender, target, message)
  end)
end, {
  description = S("Write private messages"),
  params = S("<name>") .. " " ..  S("<message>"),
})

ChatCmdBuilder.new("w", function(cmd)
  cmd:sub(":target :message:text", function (sender, target, message)
    sendMessage(sender, target, message)
  end)
end, {
  description = S("Alias for /msg"),
  params = S("<name>") .. " " ..  S("<message>"),
})

ChatCmdBuilder.new("r", function(cmd)
  cmd:sub(":message:text", function (sender, message)
    if not minetest.check_player_privs(sender, { shout=true }) and betterpm.requires_shout then
      minetest.chat_send_player(sender,
        minetest.colorize("#ff0000", S("You don't have the privilege to send private messages")))
      return
    end

    if reply_to[sender] ~= nil then

      local ignored = minetest.deserialize(storage:get_string(reply_to[sender])) or {}
      if ignored[sender] or ignored[":all"] then
        minetest.chat_send_player(sender,
          minetest.colorize("#ff0000", S("You can't send messages to @1", reply_to[sender])))
        return
      end

      reply_to[reply_to[sender]] = sender
      minetest.chat_send_player(reply_to[sender],
        minetest.colorize(betterpm.targetPrefixColor, formatPrefix(betterpm.targetPrefix, sender, reply_to[sender]) ..
        minetest.colorize(betterpm.targetMsgColor, message)))
      minetest.chat_send_player(sender,
        minetest.colorize(betterpm.senderPrefixColor, formatPrefix(betterpm.senderPrefix, sender, reply_to[sender]) ..
        minetest.colorize(betterpm.senderMsgColor, message)))
    else
      minetest.chat_send_player(sender, S("You must write to someone before replying. Use /msg <name> <message>"))
    end
  end)
end, {
  description = S("Reply to a private message"),
  params = S("<message>"),
})

-- ignore private messages
ChatCmdBuilder.new("pmignore", function(cmd)
  -- ignore everyone
  cmd:sub("", function (sender)
    local ignored = minetest.deserialize(storage:get_string(sender)) or {}
    if ignored[":all"] then
      ignored[":all"] = false
      minetest.chat_send_player(sender, S("You are no longer ignoring all private messages"))
    else
      ignored[":all"] = true
      minetest.chat_send_player(sender, S("You are now ignoring all private messages"))
    end
    storage:set_string(sender, minetest.serialize(ignored))
  end)

  cmd:sub(":name", function (sender, name)
    local ignored = minetest.deserialize(storage:get_string(sender)) or {}
    if ignored[name] then
      ignored[name] = false
      minetest.chat_send_player(sender, S("You are no longer ignoring private messages from @1", name))
    else
      ignored[name] = true
      minetest.chat_send_player(sender, S("You are now ignoring private messages from @1", name))
    end
    storage:set_string(sender, minetest.serialize(ignored))
  end)
end, {
  description = S("Ignore private messages from specific players or from everyone (/pmignore)"),
  params = S("<name>"),
})

minetest.log("action", "[BETTERPM] Mod initialised")
