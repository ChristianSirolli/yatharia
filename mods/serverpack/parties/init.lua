parties = {
    version = "1.3.0",
    path = minetest.get_modpath(minetest.get_current_modname())
}

dofile(parties.path .. "/api.lua")
dofile(parties.path .. "/callbacks.lua")
dofile(parties.path .. "/chatcmdbuilder.lua")
dofile(parties.path .. "/commands.lua")
dofile(parties.path .. "/player_manager.lua")

minetest.log("action", "[PARTIES] Mod initialised, running version " .. parties.version)
