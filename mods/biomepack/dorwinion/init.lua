dorwinion = {
    path = minetest.get_modpath("dorwinion")
}

dofile(dorwinion.path.."/nodes.lua")
dofile(dorwinion.path.."/mapgen.lua")
dofile(dorwinion.path.."/moreblocks.lua")
dofile(dorwinion.path.."/walls.lua")
