frost_land = {
    path = minetest.get_modpath("frost_land")
}

dofile(frost_land.path.."/nodes.lua")
dofile(frost_land.path.."/mapgen.lua")
dofile(frost_land.path.."/fireflies.lua")
dofile(frost_land.path.."/moreblocks.lua")
dofile(frost_land.path.."/crafting.lua")
