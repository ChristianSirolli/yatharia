bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_steppe", {"ebiomes:grass_steppe_1", "ebiomes:grass_steppe_2", "ebiomes:grass_steppe_3", "ebiomes:grass_steppe_4", "ebiomes:grass_steppe_5"},
		{"ebiomes:altai_tulip", "ebiomes:blue_allium", "ebiomes:blue_allium_purple"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_steppe_warm", {"ebiomes:grass_steppe_warm_1", "ebiomes:grass_steppe_warm_2", "ebiomes:grass_steppe_warm_3", "ebiomes:grass_steppe_warm_4", "ebiomes:grass_steppe_warm_5"},
		{"ebiomes:russian_iris", "flowers:dandelion_yellow", "flowers:dandelion_white"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_steppe_cold", {"ebiomes:grass_steppe_cold_1", "ebiomes:grass_steppe_cold_2", "ebiomes:grass_steppe_cold_3", "ebiomes:grass_steppe_cold_4", "ebiomes:grass_steppe_cold_5"},
		{"ebiomes:mountain_lily", "ebiomes:siberian_lily"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_med", {"ebiomes:grass_med_1", "ebiomes:grass_med_2", "ebiomes:grass_med_3", "ebiomes:grass_med_4", "ebiomes:grass_med_5"},
		{"ebiomes:black_iris", "ebiomes:cladanthus", "ebiomes:savory", "ebiomes:staehelina", "ebiomes:larkspur", "ebiomes:buttercup_persian", "ebiomes:chrysanthemum_yellow"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_cold", {"ebiomes:grass_cold_1", "ebiomes:grass_cold_2", "ebiomes:grass_cold_3", "ebiomes:grass_cold_4", "ebiomes:grass_cold_5"},
		{"flowers:dandelion_yellow", "flowers:dandelion_white", "flowers:tulip", "flowers:tulip_black", "flowers:chrysanthemum_green"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_swamp", {"ebiomes:grass_swamp_1", "ebiomes:grass_swamp_2", "ebiomes:grass_swamp_3", "ebiomes:grass_swamp_4", "ebiomes:grass_swamp_5"},
		{"ebiomes:marsh_stitchwort", "ebiomes:marsh_grass_yellow", "ebiomes:marsh_grass_green"} }
})

bonemeal:add_deco({
	{"ebiomes:peat_with_swamp_moss_yellow", {"ebiomes:grass_bog_1", "ebiomes:grass_bog_2", "ebiomes:grass_bog_3", "ebiomes:grass_bog_4", "ebiomes:grass_bog_5", "ebiomes:reeds"},
		{"ebiomes:cranberry_patch", "ebiomes:sundew", "ebiomes:marigold", "ebiomes:marsh_stitchwort", "ebiomes:marsh_grass_yellow", "ebiomes:marsh_grass_green"} }
})

bonemeal:add_deco({
	{"ebiomes:peat_wet_with_swamp_moss_green", {"ebiomes:reeds"},
		{"ebiomes:cranberry_patch", "ebiomes:sundew", "ebiomes:marigold"} }
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_grass_warm", {"ebiomes:grass_warm_1", "ebiomes:grass_warm_2", "ebiomes:grass_warm_3", "ebiomes:grass_warm_4", "ebiomes:grass_warm_5"},
		{"flowers:dandelion_yellow", "flowers:dandelion_white", "flowers:tulip", "flowers:chrysanthemum_green", "ebiomes:larkspur"} }
})

bonemeal:add_deco({
	{"ebiomes:dry_dirt_with_grass_arid", {"ebiomes:grass_arid_1", "ebiomes:grass_arid_2", "ebiomes:grass_arid_3", "ebiomes:grass_arid_4", "ebiomes:grass_arid_5"},
		{"default:dry_shrub"} }
})

bonemeal:add_deco({
	{"ebiomes:dry_dirt_with_grass_arid_cool", {"ebiomes:grass_arid_cool_1", "ebiomes:grass_arid_cool_2", "ebiomes:grass_arid_cool_3", "ebiomes:grass_arid_cool_4", "ebiomes:grass_arid_cool_5"},
		{"default:dry_shrub"} }
})

bonemeal:add_sapling({
	{"ebiomes:cowberry_bush_sapling", ebiomes.grow_new_cowberry, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:blackcurrant_bush_sapling", ebiomes.grow_new_blackcurrant, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:gooseberry_bush_sapling", ebiomes.grow_new_gooseberry, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:redcurrant_bush_sapling", ebiomes.grow_new_redcurrant, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:thorn_bush_sapling", ebiomes.grow_new_thorn_bush, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:peashrub_sapling", ebiomes.grow_new_peashrub, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:afzelia_sapling", ebiomes.grow_new_afzelia_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:limba_sapling", ebiomes.grow_new_limba_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:siri_sapling", ebiomes.grow_new_siri_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:tamarind_sapling", ebiomes.grow_new_tamarind_tree, "soil"},
})

bonemeal:add_deco({
	{"ebiomes:dry_dirt_with_humid_savanna_grass", {"ebiomes:humid_savanna_grass_1", "ebiomes:humid_savanna_grass_2", "ebiomes:humid_savanna_grass_3", "ebiomes:humid_savanna_grass_4", "ebiomes:humid_savanna_grass_5"},
		{"ebiomes:jaragua_grass"} }
})

bonemeal:add_sapling({
	{"ebiomes:willow_sapling", ebiomes.grow_new_willow_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:alder_sapling", ebiomes.grow_new_alder_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:ash_sapling", ebiomes.grow_new_ash_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:birch_sapling", ebiomes.grow_new_birch_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:cypress_sapling", ebiomes.grow_new_cypress, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:olive_sapling", ebiomes.grow_new_olive, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:downy_birch_sapling", ebiomes.grow_new_downy_birch_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:pear_sapling", ebiomes.grow_new_pear_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:quince_sapling", ebiomes.grow_new_quince_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:chestnut_sapling", ebiomes.grow_new_chestnut_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:hornbeam_sapling", ebiomes.grow_new_hornbeam_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:sugi_sapling", ebiomes.grow_new_sugi_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:mizunara_sapling", ebiomes.grow_new_mizunara_tree, "soil"},
})

bonemeal:add_sapling({
	{"ebiomes:stoneoak_sapling", ebiomes.grow_new_stoneoak_tree, "soil"},
})

bonemeal:add_deco({
	{"ebiomes:dirt_with_japanese_rainforest_litter", {"default:fern_1", "default:fern_2", "default:fern_3"},
		{"ebiomes:forestgrowth", "ebiomes:moss"} }
})