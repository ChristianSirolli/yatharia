-- Water drips
-- Based on the Dripping Water Mod by kddekadenz

--Random
--TODO: Get proper random seed based on world seed or something like that
math.randomseed(4053633)

--Drop entities
minetest.register_entity("pedology:drop_water", {
	hp_max = 2000,
	physical = true,
	collisionbox = {0,0,0,0,0,0},
	visual = "cube",
	visual_size = {x=0.05, y=0.1},
	textures = {"pedology_water_fresh.png", "pedology_water_fresh.png", "pedology_water_fresh.png", "pedology_water_fresh.png", "pedology_water_fresh.png", "pedology_water_fresh.png"},
	spritediv = {x=1, y=1},
	initial_sprite_basepos = {x=0, y=0},

	on_activate = function(self, staticdata)
		self.object:setsprite({x=0,y=0}, 1, 1, true)
	end,

	on_step = function(self, dtime)
	local k = math.random(1,222)
	local ownpos = self.object:getpos()

	if k==1 then
	self.object:setacceleration({x=0, y=-5, z=0})
	end

	if minetest.env:get_node({x=ownpos.x, y=ownpos.y +0.5, z=ownpos.z}).name == "air" then
	self.object:setacceleration({x=0, y=-5, z=0})
	end
	
		if minetest.env:get_node({x=ownpos.x, y=ownpos.y -0.5, z=ownpos.z}).name ~= "air" then
		self.object:remove()
		minetest.sound_play({name="pedology_drip"}, {pos = ownpos, gain = 0.5, max_hear_distance = 8})
		end
	end,
})

function pedology.create_drip(pos)
	if minetest.env:get_node({x=pos.x, y=pos.y -1, z=pos.z}).name == "air" and 
	minetest.env:get_node({x=pos.x, y=pos.y -2, z=pos.z}).name == "air" then
		local i = math.random(-45,45) / 100
		minetest.env:add_entity({x=pos.x + i, y=pos.y - 0.5, z=pos.z + i}, "pedology:drop_water")
	end
end

