local S
if (minetest.global_exists("intllib")) then
	S = intllib.Getter()
else
	S = function ( s ) return s end
end

pedology = {
	path = minetest.get_modpath('pedology')
}
pedology.wetnames = {[0] = "dry",  "wet", "watery", "sludgy", "muddy", "slurry" }

dofile(pedology.path .. "/settings.lua")

if pedology.USE_DRIPS == 1 then
	dofile(pedology.path .. "/drip.lua")
end

--[[ ABMs ]]

function pedology.ooze(pos, node, active_object_count, active_object_wider)
	local wet = minetest.get_item_group(node.name, "wet")
	if wet == 0 then return end

	--[[ This table contain the nodes which are either neighbors
	of node or which touch the node at an edge or corner and which are
	not on a heigher y-level than
	of node
	]]
	local neighbors = {
		{	-- neighbor directly below
			{x=pos.x, y=pos.y-1, z=pos.z},
			minwet=1
		},
		
		{	-- touching the lower sides
			{x=pos.x, y=pos.y-1, z=pos.z-1},
			{x=pos.x, y=pos.y-1, z=pos.z+1},
			{x=pos.x-1, y=pos.y-1, z=pos.z},
			{x=pos.x+1, y=pos.y-1, z=pos.z},
			minwet=1
		},
		{	-- touching the lower corners
			{x=pos.x-1, y=pos.y-1, z=pos.z-1},
			{x=pos.x-1, y=pos.y-1, z=pos.z+1},
			{x=pos.x-1, y=pos.y-1, z=pos.z-1},
			{x=pos.x+1, y=pos.y-1, z=pos.z+1},
			{x=pos.x+1, y=pos.y-1, z=pos.z+1},
			minwet=1
		},
		{	-- sides on the same level
			{x=pos.x-1, y=pos.y, z=pos.z}, -- left
			{x=pos.x+1, y=pos.y, z=pos.z}, -- right
			{x=pos.x, y=pos.y, z=pos.z-1}, -- before
			{x=pos.x, y=pos.y, z=pos.z+1}, -- behind
			minwet=2
		},
		{	-- touching at the side on the same level
			{x=pos.x-1, y=pos.y, z=pos.z-1},
			{x=pos.x-1, y=pos.y, z=pos.z+1},
			{x=pos.x+1, y=pos.y, z=pos.z-1},
			{x=pos.x+1, y=pos.y, z=pos.z+1},
			minwet=2
		},
	}

	for i=1,#neighbors do
		for j=1,#neighbors[i] do
			local node = minetest.get_node(neighbors[i][j])
			local name = node.name
			local destwet = minetest.get_item_group(name, "wet")
			if(
				(destwet < wet) and 
				(minetest.get_item_group(name, "sucky") >= 1)) and
				(neighbors[i].minwet <= (wet - destwet)) then
					pedology.wetten(neighbors[i][j])
					pedology.dry(pos)
					return
			end
		end
	end
end

--[[ This is an ABM action which wettens the node iff many water nodes
are neighbors of it or touching it at an edge or corner.]]
function pedology.suck(pos, node, active_object_count, active_object_wider)
	local wet = minetest.get_item_group(node.name, "wet")

	local score = 0

	-- These three tables contain the positions of all nodes around node
	local neighbors= {
		{ -- sides
		{x=pos.x, y=pos.y-1, z=pos.z}, -- below
		{x=pos.x-1, y=pos.y, z=pos.z}, -- left
		{x=pos.x+1, y=pos.y, z=pos.z}, -- right
		{x=pos.x, y=pos.y+1, z=pos.z}, -- above
		{x=pos.x, y=pos.y, z=pos.z-1}, -- before
		{x=pos.x, y=pos.y, z=pos.z+1}, -- behind
		rating = 1
		},
		{ -- edges
		{x=pos.x, y=pos.y-1, z=pos.z-1},
		{x=pos.x, y=pos.y-1, z=pos.z+1},
		{x=pos.x, y=pos.y+1, z=pos.z-1},
		{x=pos.x, y=pos.y+1, z=pos.z+1},
		{x=pos.x-1, y=pos.y, z=pos.z-1},
		{x=pos.x-1, y=pos.y, z=pos.z+1},
		{x=pos.x+1, y=pos.y, z=pos.z-1},
		{x=pos.x+1, y=pos.y, z=pos.z+1},
		{x=pos.x-1, y=pos.y-1, z=pos.z},
		{x=pos.x-1, y=pos.y+1, z=pos.z},
		{x=pos.x+1, y=pos.y-1, z=pos.z},
		{x=pos.x+1, y=pos.y+1, z=pos.z},
		rating=0.5
		},
		{ -- corners	
		{x=pos.x-1, y=pos.y-1, z=pos.z-1},
		{x=pos.x-1, y=pos.y-1, z=pos.z+1},
		{x=pos.x-1, y=pos.y+1, z=pos.z-1},
		{x=pos.x-1, y=pos.y+1, z=pos.z+1},
		{x=pos.x-1, y=pos.y-1, z=pos.z-1},
		{x=pos.x+1, y=pos.y+1, z=pos.z+1},
		{x=pos.x+1, y=pos.y+1, z=pos.z-1},
		{x=pos.x+1, y=pos.y-1, z=pos.z+1},
		{x=pos.x+1, y=pos.y-1, z=pos.z+1},
		rating=0.25
		}
	}

	for i=1,#neighbors do
		for j=1,#neighbors[i] do
			local neighbor = minetest.get_node(neighbors[i][j])
			local name = neighbor.name
			local water = minetest.get_item_group(name, "water")
			if(water >= 1) then
				score = score + neighbors[i].rating
			end
		end
	end

	local wetten = false
	if(wet == 0 and score >= 1) then
		wetten=true
	elseif(wet == 1 and score >= 2) then
		wetten= true
	elseif(wet == 2 and score >= 3) then
		wetten=true
	elseif(wet == 3 and score >= 5) then
		wetten=true
	elseif(wet == 4 and score >= 8) then
		wetten=true
	elseif(wet == 5 and score >= 11) then
		-- erosion: turn node into water
		-- TODO: Also drop the drops
		minetest.set_node(pos, {name="pedology:water_fresh_source"})
		return
	end

	if(wetten==true) then
		pedology.wetten(pos)
	end
end

--[[ ABM action which dries out a node when it is in direct sunlight ]]
function pedology.sun_dry(pos, node, active_object_count, active_object_wider)
	if(minetest.get_item_group(node.name, "wet") < 6) then
		-- don’t dry off if there is water nearby
		if(not minetest.find_node_near(pos, 1, {"group:water"})) then
			local light = minetest.get_node_light(pos, minetest.get_timeofday())
			if light >= pedology.DRY_LIGHT then
				pedology.dry(pos)
			end
		end
	end
end


--[[ liquids ]]

function pedology.register_liquid(basename, description, alpha, viscosity, drowning, damage_per_second, post_effect_color, additional_groups)
	local flowing_groups = {wet=6,liquid=1,not_in_creative_inventory=1}
	local source_groups = {wet=6,liquid=1}
	for k,v in pairs(additional_groups) do
		flowing_groups[k] = v
		source_groups[k] = v
	end

	local tile = "pedology_"..basename..".png"
	local nodedef_source = {
	description = S(description.." source"),
	inventory_image = minetest.inventorycube(tile),
	tiles = {tile},
	special_tiles = {{name=tile, backface_culling=false}},
	-- alpha = alpha,
	use_texture_alpha = 'blend',
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	drop = "",
	liquid_alternative_flowing = "pedology:"..basename.."_flowing",
	liquid_alternative_source = "pedology:"..basename.."_source",
	liquid_viscosity = viscosity,
	post_effect_color = post_effect_color,
	drowning = drowning,
	damage_per_second = damage_per_second,

	drawtype = "liquid",
	liquidtype = "source",
	groups = source_groups,
	}
	minetest.register_node("pedology:"..basename.."_source", nodedef_source)

	local nodedef_flowing = {
	description = S("flowing "..description),
	inventory_image = minetest.inventorycube(tile),
	tiles = {tile},
	special_tiles = {{name=tile, backface_culling=false}, {name=tile, backface_culling=true}},
	-- alpha = alpha,
	use_texture_alpha = 'blend',
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	drop = "",
	liquid_alternative_flowing = "pedology:"..basename.."_flowing",
	liquid_alternative_source = "pedology:"..basename.."_source",
	liquid_viscosity = viscosity,
	post_effect_color = post_effect_color,
	drowning = drowning,
	damage_per_second = damage_per_second,

	drawtype = "flowingliquid",
	liquidtype = "flowing",
	groups = flowing_groups,
	paramtype2 = "flowingliquid",
	}

	minetest.register_node("pedology:"..basename.."_flowing", nodedef_flowing)

end

pedology.register_liquid("water_fresh", "freshwater", 100, 1,   2, 0, {a=60, r=100, b=200, g=100}, {water=1})
--pedology.register_liquid("water_salt", "salt water",  180, 2,   4, 0, {a=80, r=120, b=200, g=120}, {water=1})
pedology.register_liquid("water_ponding", "ponding water", 234, 4, 3, 0, {a=128, r=85, g=85, g=60}, {water=1})

--[[ Water drip function ]]
function pedology.drip_particle(pos, wet)
	local below1 = minetest.get_node({x=pos.x, y=pos.y-1, z=pos.z})
	local below2 = minetest.get_node({x=pos.x, y=pos.y-2, z=pos.z})
	if(below1.name == "air" and below2.name == "air") then
		return minetest.add_particlespawner(
			wet,	-- amount
			0,	-- time
			{ x=pos.x -(45/100), y=pos.y - 0.5, z=pos.z -(45/100) },	-- minpos
			{ x=pos.x +(45/100), y=pos.y - 0.5, z=pos.z +(45/100) },	-- maxpos
			{ x=0, y=0, z=0 }, { x=0, y=0, z=0 }, -- minvel, maxvel
			{ x=0, y=-9.82, z=0 }, { x=0, y=-9.80, z=0 }, -- minacc, maxacc
			1, 3, -- minexptime, maxexptime
			1, 1, -- minsize, maxsize
			false, -- collisiondetection
			"pedology_water_fresh.png" -- texture
			-- playername
		--[[ For future Lua API
		{
			amount = wet,
			time = 0,
			minpos = { pos.x -(45/100), pos.y - 0.5, pos.z -(45/100) },
			maxpos = { pos.x +(45/100), pos.y - 0.5, pos.z +(45/100) },
			minvel = { 0, -9.81, 0 },
			maxvel = { 0, 0, 0 },
			minexptime = 1,
			maxexptime = 3,
			collisiondetection = true,
			vertical = true,
			texture = "pedology_water_fresh.png",
		}]]
)
	else
		return nil
	end
end
 
--[[ register a sucky/oozing node to this mod ]]
function pedology.register_sucky(basename, description, wetness, oozing, sucky, melting_point, drop, sounds, additional_groups)
	local wetname = basename.."_"..tostring(wetness)
	local noncreative
	local groups = { sucky=sucky, oozing=oozing, wet=wetness, melting_point=melting_point, not_in_creative_inventory = noncreative, [basename]=1 }
	if additional_groups ~= nil then
		for k,v in pairs(additional_groups) do
			groups[k] = v
		end
	end

	local name = "pedology:"..wetname
	local itemname

	-- If the node is not dry, do not add it into the creative inventory
	if wetness == 0 then noncreative = 0 else noncreative = 1 end
	local on_construct, on_destruct
	if pedology.USE_DRIPS == 2 then
		on_construct = function(pos)
			local dripper = pedology.drip_particle(pos, wetness)
			if(dripper ~= nil) then
				local meta = minetest.get_meta(pos)
				meta:set_int("dripper", dripper)
			end
		end
		on_destruct = function(pos)
			local meta = minetest.get_meta(pos)
			local dripper = meta:get_int("dripper")
			if(dripper ~= nil) then
				minetest.delete_particlespawner(dripper)
			end
		end
	else
		on_construct, on_destruct = nil
	end
	local nodedef = {
		description = S(description),
		inventory_image = minetest.inventorycube("pedology_"..wetname..".png"),
		tiles = {"pedology_"..wetname..".png"},
		paramtype = "light",
		drop = drop,
		groups = groups,
		sounds = sounds,
		is_ground_content = true,
		on_construct = on_construct,
		on_destruct = on_destruct,
	}
	minetest.register_node(name, nodedef)



end


--[[
	This function registers a couple of sucky/oozing nodes, including all their “wet” variants.
	It also registers an ABM.

	parameters:
	basename: The internal name piece from which the concrete basenames will be build. The wetness level will be appended.
	basedescription. The description of the nodes. A proper wetness adjective (“dry”, “wet”, …) will be prepended.
	lumpdescription. The description component of the “lump” of the node. If nil, no lump is used
	maxwet: The maximum wetness level of this node group (minimum: 0, maximum: 5)
	maxlump: The maximum wetness level of lumps of this node group (minimum: 0, recommended maximum: 3)
	oozeinterval: The interval in seconds in which it the node may ooze. the Minimal value: 1
	oozechance: The inverted chance (1/x) to ooze
	melttable: Table of melting_point values for each wetness level
	dropcount: How many lumps nodes of this group drop. If 0, the nodes simply drop themselves (not recommended!)
	sounds: Sound specification for all nodes
	additional_groups: Either a table of tables of strings, a table of strings or nil. If it is nil, no additional groups will be added to all nodes. If it is a table, the strings are interpreted as group names which will be added to each node. If it is a table of tables strings, you can specify the groups tables for each wetness level (start with index number 0 for dry).
]]
function pedology.register_sucky_group(basename, basedescription, lumpbasedescription, maxwet, maxlump, oozeinterval, oozechance, melttable, dropcount, sounds, additional_groups)
	local sucky, oozing, dripinterval
	local m -- melting_point
	local lumpdescription 
	local groups
	local nestedgroups = false
	if(type(additional_groups) == "table") then
		if(type(additional_groups[0]) == "table") then
			nestedgroups = true
		end
	end

	for w=0, maxwet do
		if(w==0) then oozing=0 else oozing=1 end
		if (w==maxwet and w ~= 5) then sucky=0 else sucky=1 end
		if melttable == nil then m = 0 else m = melttable[w] end
		if( lumpbasedescription ~= nil) then
			lumpdescription = S((pedology.wetnames[w]).." "..lumpbasedescription)
		else
			lumpdescription = nil
		end

		local itemname, drop, itemdef
		if( maxlump == -1) then
			drop = nil
		elseif( w > maxlump) then
			itemname = "pedology:lump_"..basename.."_"..tostring(maxlump)
			drop = itemname .. " " .. tostring(dropcount)
		else
			itemname = "pedology:lump_"..basename.."_"..tostring(w)
			drop = itemname .. " " .. tostring(dropcount)
			itemdef = {
				description = S(lumpdescription),
				inventory_image = "pedology_lump_inv_"..basename.."_"..tostring(w)..".png",
				wield_image = "pedology_lump_"..basename.."_"..tostring(w)..".png"
			}
			minetest.register_craftitem("pedology:lump_"..basename.."_"..tostring(w), itemdef)
		end
		if(nestedgroups) then
			groups = additional_groups[w]
		else
			groups = additional_groups
		end
		if(w==3) then
			groups["disable_jump"] = 1
		end
		pedology.register_sucky(basename, S((pedology.wetnames[w]).." "..basedescription), w, oozing, sucky, m, drop, sounds, groups)
		-- register dripping
		if(w>0 and pedology.USE_DRIPS == 1) then
			minetest.register_abm({
				nodenames = {"pedology:"..basename.."_"..tostring(w)},
				neighbors = {"air"},
				interval = math.max(oozeinterval/4,2),
				chance = math.max(oozechance,22),
				action = pedology.create_drip,
			})
		end
	end
	if(maxwet > 0) then
		minetest.register_abm({
			nodenames = {"group:"..basename},
			neighbors = {"group:sucky"},
			interval = oozeinterval,
			chance = oozechance,
			action = pedology.ooze,
		})

	end
end

do

local sound_clay = {footstep = {name="pedology_clay_footstep", gain=0.5}}
local sound_silt_fine = {footstep = {name="pedology_silt_footstep", gain=0.3}}
local sound_silt_medium = {footstep = {name="pedology_silt_footstep", gain=0.35}}
local sound_silt_coarse = {footstep = {name="pedology_silt_footstep", gain=0.4}}
local sound_sand_fine = {footstep = {name="pedology_sand_footstep", gain=0.2}}
local sound_sand_medium = {footstep = {name="pedology_sand_footstep", gain=0.25}}
local sound_sand_coarse = {footstep = {name="pedology_sand_footstep", gain=0.3}}
local sound_gravel_fine = {footstep = {name="pedology_gravel_footstep", gain=1}}
local sound_gravel_medium = {footstep = {name="pedology_gravel_footstep", gain=1}}
local sound_gravel_coarse = {footstep = {name="pedology_gravel_footstep", gain=1}}
local sound_turf_sapric = {footstep = {name="pedology_turf_footstep", gain=0.2}}
local sound_turf_hemic = {footstep = {name="pedology_turf_footstep", gain=0.24}}
local sound_turf_fibric = {footstep = {name="pedology_turf_footstep", gain=0.28}}

--[[ register sucky and oozing nodes ]]
--[[ ground nodes ]]
pedology.register_sucky_group("clay", "clay", "clay lump",
	3, 3, 60, 1.25, {3000, 3100, 3200, 3500, 3550, 3600}, 4, sound_clay, { crumbly = 3, sun_dry = 1})
pedology.register_sucky_group("silt_fine", "fine silt", "fine silt lump",
	3, 3, 45, 1.25, {2800, 2900, 3000, 3100, 3200, 3300}, 4, sound_silt_fine, { crumbly = 3, sun_dry = 1 })
pedology.register_sucky_group("silt_medium", "medium silt", "medium silt lump",
	3, 3, 30, 1.25, {2600, 2800, 3200, 3800, 4200}, 4, sound_silt_medium, { crumbly = 3, sun_dry = 1 })
pedology.register_sucky_group("silt_coarse", "coarse silt", "coarse silt lump",
	3, 3, 20, 1.25, {2000, 2200, 2400, 2800}, 2, sound_silt_coarse, { crumbly = 3, sun_dry = 1 })
pedology.register_sucky_group("sand_fine", "fine sand", "fine sand pile",
	2, 0, 10, 1.111111, {1100, 1200, 1300}, 2, sound_sand_fine, { crumbly = 3, sand = 1, sun_dry = 1 })
pedology.register_sucky_group("sand_medium", "medium sand", "medium sand pile",
	1, 0, 5, 1.111111, {990, 1100}, 2, sound_sand_medium, { crumbly = 3, sand = 1, sun_dry = 1 })
pedology.register_sucky_group("sand_coarse", "coarse sand", "coarse sand pile",
	0, 0, nil, nil, {900}, 1, sound_sand_coarse, { crumbly = 3, sand = 1, sun_dry = 1 })
pedology.register_sucky_group("gravel_fine", "fine gravel", "pebble",
	1, 0, 2, 1, {670, 770}, 9, sound_gravel_fine, { crumbly = 2, sun_dry = 1 })
pedology.register_sucky_group("gravel_medium", "medium gravel", "medium stone",
	2, 0, 1.5, 1, {600, 800, 1250}, 3, sound_gravel_medium, { crumbly = 2, sun_dry = 1})
pedology.register_sucky_group("gravel_coarse", "coarse gravel", "big stone",
	2, 0, 1, 1, {500, 750, 1000}, 1, sound_gravel_coarse, { crumbly = 1, sun_dry = 1 })

--[[ snow and ice ]]
minetest.register_craftitem("pedology:lump_snow", {
	description = S("snow ball"),
	wield_image = "pedology_lump_snow.png",
	inventory_image = "pedology_lump_snow.png",
})

minetest.register_craftitem("pedology:lump_ice_pure", {
	description = S("small pure ice cube"),
	wield_image = "pedology_lump_ice_pure.png",
	inventory_image = "pedology_lump_ice_pure.png",
})

minetest.register_craftitem("pedology:lump_ice_white", {
	description = S("small white ice cube"),
	wield_image = "pedology_lump_ice_white.png",
	inventory_image = "pedology_lump_ice_white.png",
})

-- TODO: slushy snow is very instable snow; it should be destroyed when touched
minetest.register_node("pedology:snow_slush", {
	description = S("slushy snow"),
	tiles = { "pedology_snow_slush.png" },
	sounds = { footstep = { name = "pedology_snow_soft_footstep", gain = 1 } },
	is_ground_content = true,
	groups = { crumbly = 1, level = 1, melts = 1, falling_node = 1},
	drop = "",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:snow_soft", {
	description = S("soft snow"),
	tiles = { "pedology_snow_soft.png" },
	sounds = { footstep = { name = "pedology_snow_soft_footstep", gain = 1 } },
	is_ground_content = true,
	groups = { crumbly = 2, level = 2, melts = 1, falling_node = 1 },
	drop = "pedology:lump_snow 1",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:snow", {
	description = S("snow"),
	tiles = { "pedology_snow.png" },
	sounds = { footstep = { name = "pedology_snow_footstep", gain = 0.9 } },
	is_ground_content = true,
	groups = { crumbly = 3, level = 3, melts = 1, falling_node = 1 },
	drop = "pedology:lump_snow 2",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:snow_packing", {
	description = S("packing snow"),
	tiles = { "pedology_snow_packing.png" },
	sounds = { footstep = { name = "pedology_snow_footstep", gain = 1 } },
	is_ground_content = true,
	groups = { crumbly = 3, level = 3, melts = 1 },
	drop = "pedology:lump_snow 3",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:snow_firn", {
	description = S("firn"),
	tiles = { "pedology_snow_firn.png" },
	sounds = { footstep = { name = "pedology_ice_white_footstep", gain = 0.7 } },
	is_ground_content = true,
	groups = { crumbly = 2, cracky = 2, level = 4, melts = 1 },
	drop = "pedology:lump_snow 4",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:ice_white", {
	description = S("white ice"),
	tiles = { "pedology_ice_white.png" },
	sounds = { footstep = { name = "pedology_ice_white_footstep", gain = 0.8 } },
	is_ground_content = true,
	groups = { cracky = 3, level = 1, melts = 1 },
	drop = "pedology:lump_ice_white 4",
	freezemelt = "pedology:water_fresh_source",
})

minetest.register_node("pedology:ice_pure", {
	description = S("pure ice"),
	drawtype = "glasslike",
	paramtype = "light",
	tiles = { "pedology_ice_pure.png" },
	inventory_image = minetest.inventorycube( "pedology_ice_pure.png" ),
	sounds = { footstep = { name = "pedology_ice_pure_footstep", gain = 0.8 } },
	is_ground_content = true,
	groups = { cracky = 3, level = 2, melts = 1 },
	drop = "pedology:lump_ice_pure 4",
	freezemelt = "pedology:water_fresh_source",
})

--[[ mud and slurry ]]
pedology.register_liquid("clay_4", "clay mud", 240, 6, 5, 0, {a=240, r=123, g=57, b=6}, {sucky=1, oozing=1, wet=4})
pedology.register_liquid("clay_5", "clay slurry", 176, 4, 5, 0, {a=176, r=146, g=85, b=73}, {oozing=1, wet=5})

pedology.register_liquid("silt_fine_4", "fine silt mud", 230, 6, 5, 0, {a=230, r=137, g=82, b=0}, {sucky=1, oozing=1, wet=4})
pedology.register_liquid("silt_fine_5", "fine silt slurry", 166, 4, 5, 0, {a=166, r=179, g=117, b=59}, {oozing=1, wet=5})

pedology.register_liquid("silt_medium_4", "medium silt mud", 220, 6, 5, 0, {a=220, r=165, g=131, b=0}, {oozing=1, wet=4})

--[[ turf ]]
pedology.register_sucky_group("turf_fibric", "fibric turf", "fibric turf cutting",
	2, 2, 120, 1.25, {1000, 1100, 1200}, 4, sound_turf_fibric, { [0] = { crumbly = 3, flammable = 1 }, { crumbly = 3 }, {crumbly = 3} } )
pedology.register_sucky_group("turf_hemic", "hemic turf", "hemic turf cutting",
	2, 2, 180, 1.225, {1100, 1200, 1300}, 3, sound_turf_hemic, { [0] = { crumbly = 3, flammable = 1 }, { crumbly = 3 }, { crumbly = 3 } }  )
pedology.register_sucky_group("turf_sapric", "sapric turf", "sapric turf cutting",
	2, 2, 240, 1.2, {1200, 1300, 1400}, 2, sound_turf_sapric, { [0] = { crumbly = 3, flammable = 1 }, { crumbly = 3 }, { crumbly = 3 } } )

minetest.register_craft({
	type = "fuel",
	recipe = "pedology:turf_fibric_0",
	burntime = 100,
})
minetest.register_craft({
	type = "fuel",
	recipe = "pedology:turf_hemic_0",
	burntime = 200,
})
minetest.register_craft({
	type = "fuel",
	recipe = "pedology:turf_sapric_0",
	burntime = 300,
})

minetest.register_craft({
	type = "fuel",
	recipe = "pedology:lump_turf_fibric_0",
	burntime = 10,
})
minetest.register_craft({
	type = "fuel",
	recipe = "pedology:lump_turf_hemic_0",
	burntime = 20,
})
minetest.register_craft({
	type = "fuel",
	recipe = "pedology:lump_turf_sapric_0",
	burntime = 30,
})


end

pedology.maxwet = {}
pedology.maxwet.clay =		5
pedology.maxwet.silt_fine =	5
pedology.maxwet.silt_medium =	4
pedology.maxwet.silt_coarse =	3
pedology.maxwet.sand_fine =	2
pedology.maxwet.sand_medium =	1
pedology.maxwet.sand_coarse =	0
pedology.maxwet.gravel_fine =	1
pedology.maxwet.gravel_medium =	2
pedology.maxwet.gravel_coarse =	2

pedology.maxwet.turf_fibric =    2
pedology.maxwet.turf_hemic =     2
pedology.maxwet.turf_sapric =    2

--[[ “API” functions ]]
--[[ Replace the node at <pos> with a node with a “drier” version of it, if available ]]
function pedology.dry(pos)
	local node = minetest.get_node(pos)
	local wet = minetest.get_item_group(node.name, "wet")
	local dried = tostring(wet - 1)
	if wet <= 0 then
		return
	end
	local newbasename = string.sub(node.name,1,#node.name-1)
	newbasename = newbasename .. dried
	minetest.set_node(pos, {name=newbasename, param1=0, param2=0})
end

--[[ Replace the node at <pos> with a “wetter” version of it, if available ]]
function pedology.wetten(pos)
	local node = minetest.get_node(pos)
	local wet = minetest.get_item_group(node.name, "wet")
	local cutbasename = string.sub(node.name,10,#node.name-2)
	if pedology.maxwet[cutbasename] == nil then
		return
	elseif wet > pedology.maxwet[cutbasename] then
		return
	end
	
	local newbasename = string.sub(node.name,1,#node.name-1) .. tostring(wet+1)
	minetest.set_node(pos, {name=newbasename, param1=0, param2=0})
end

--[[ register ABMs ]]
minetest.register_abm({
	nodenames = {"group:sun_dry"},
	neighbors = {"air"},
	interval = 30,
	chance = 10,
	action = pedology.sun_dry,
})

minetest.register_abm({
	nodenames = {"group:sucky"},
	neighbors = {"group:water"},
	interval = 30,
	chance = 1,
	action = pedology.suck,
})

minetest.register_abm({
	nodenames = {"group:melting_point"},
	neighbors = {"group:hot"},
	interval = 1,
	chance = 1,
	action = function(pos,node,...)
		minetest.set_node(pos,node)
	end,
})
