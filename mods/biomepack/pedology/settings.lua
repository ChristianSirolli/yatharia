--[[ Create water drips. This is still an experimental feature
 0 = don’t use water drips
 1 = use entity-based water drips (3D, with sounds, slow)
 2 = use particle-based water drips (2D, without sounds, fast) ]]
pedology.USE_DRIPS = 0

-- Minimum light level at which nodes dry out from light
pedology.DRY_LIGHT = 13
