nightshade = {
    path = minetest.get_modpath("nightshade")
}

local path = nightshade.path

dofile(path .. "/nodes.lua")
dofile(path .. "/mapgen.lua")
dofile(path .. "/crafting.lua")
dofile(path .. "/fireflies.lua")
dofile(path .. "/moreblocks.lua")
