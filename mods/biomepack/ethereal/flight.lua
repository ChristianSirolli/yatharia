local timer_check = 5 -- seconds per check
local S = ethereal.intllib
local is_50 = minetest.has_feature("object_use_texture_alpha")


local function get_timer(user)

	if not user then return end

	if is_50 then

		local meta = user:get_meta()

		if not meta then return "" end

		return meta:get_string("ethereal:fly_timer") or ""
	else
		return user:get_attribute("ethereal:fly_timer") or ""
	end
end


local function has_fly(name)
	return minetest.get_player_privs(name).fly
end


local function set_timer(user, timer)

	if is_50 then

		local meta = user:get_meta() ; if not meta then return end

		meta:set_string("ethereal:fly_timer", timer)
	else
		user:set_attribute("ethereal:fly_timer", timer)
	end
end


local function set_flight(user, set)

	local name = user and user:get_player_name() ; if not name then return end
	local privs = minetest.get_player_privs(name)

	privs.fly = set

	minetest.set_player_privs(name, privs)

	-- when flight removed set timer to temp position
	if set ~= true then
		set_timer(user, "-99")
	end
end


-- after function
local function ethereal_set_flight(user)

	local name = user and user:get_player_name() ; if not name then return end

	local timer = tonumber(get_timer(user)) or 0

	-- if timer ran out then remove 'fly' privelage
	if timer <= 0 and timer ~= -99 then

		set_flight(user, nil)

		return
	end

	local privs = minetest.get_player_privs(name)

	-- have we already applied 'fly' privelage?
	if not privs.fly then
		set_flight(user, true)
	end

	-- handle timer
	timer = timer - timer_check

	-- show expiration message and play sound
	if timer <= 10 then

		minetest.chat_send_player(name,
				minetest.get_color_escape_sequence("#ff5500")
				.. S("Flight timer about to expire!"))

		minetest.sound_play("default_dig_dig_immediate",
				{to_player = name, gain = 1.0}, true)
	end

	-- set updated timer
	set_timer(user, timer)

	-- restart checks
	minetest.after(timer_check, function()
		ethereal_set_flight(user)
	end)
end


-- on join / leave
minetest.register_on_joinplayer(function(player)

	-- wait 2 seconds before doing flight checks on player
	minetest.after(2.0, function(player)

		-- get player name and timer
		local name = player and player:get_player_name() ; if not name then return end
		local timer = get_timer(player)

		-- if timer is blank and player can already fly then default and return
		if timer == "" and has_fly(name) then

			set_timer(player, "-99")

			return
		end

		timer = tonumber(timer) or 0

		-- if timer is set to default then return
		if timer == -99 then
			return
		end

		-- if we got this far and player is flying then start countdown check
		if has_fly(name) then

			minetest.after(timer_check, function()
				ethereal_set_flight(player)
			end)
		end

	end, player)
end)
