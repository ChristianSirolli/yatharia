
moretools = {
  translator = minetest.get_translator("moretools")
}

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

if minetest.global_exists("screwdriver") then
  dofile(modpath.."/screwdrivers.lua")
end

if minetest.global_exists("vines") then
  dofile(modpath.."/shears.lua")
end

if minetest.global_exists("composting") then
  dofile(modpath.."/garden_trowels.lua")
end

