--[[
	Mod Craft Table for Minetest
	Copyright (C) 2019 BrunoMine (https://github.com/BrunoMine)
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
  ]]

-- Global index
craft_table = {
	path = minetest.get_modpath("craft_table"),
	simple_craft_grid = minetest.settings:get("craft_table_2x2_craft_grid_to_players")
}

-- simple craft grid for players
if craft_table.simple_craft_grid == "false" then
	craft_table.simple_craft_grid = false
else
	craft_table.simple_craft_grid = true
end

-- API
dofile(craft_table.path .."/api.lua")

-- Craft tables
dofile(craft_table.path .."/simple_table.lua")

