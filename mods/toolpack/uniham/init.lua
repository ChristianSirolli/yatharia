-- Init
uniham = {}
uniham.modname = minetest.get_current_modname()
uniham.modpath = minetest.get_modpath(uniham.modname)
uniham.syspath = uniham.modpath..DIR_DELIM..'system'
uniham.translator = minetest.get_translator(uniham.modname)


-- Load mod
dofile(uniham.syspath..DIR_DELIM..'crush_nodes.lua')
dofile(uniham.syspath..DIR_DELIM..'register_hammer.lua')
dofile(uniham.syspath..DIR_DELIM..'registry.lua')


-- Clean up
--
-- `uniham.register_hammer` is kept available for API purposes
uniham.replacements = nil
uniham.syspath = nil
uniham.modpath = nil
uniham.crush_nodes = nil
