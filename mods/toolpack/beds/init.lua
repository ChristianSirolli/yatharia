 -- Load support for MT game translation.
local S
if minetest.get_translator ~= nil then
	S = minetest.get_translator("beds") -- 5.x translation function
else
	if minetest.global_exists("intllib") then
		dofile(intllib.path .. "/init.lua")
		if intllib.make_gettext_pair then
			gettext, ngettext = intllib.make_gettext_pair() -- new gettext method
		else
			gettext = intllib.Getter() -- old text file method
		end
		S = gettext
	else -- boilerplate function for 0.4
		S = function(str, ...)
			local args = {...}
			return str:gsub("@%d+", function(match)
				return args[tonumber(match:sub(2))]
			end)
		end
	end
end


beds = {
	mod = "redo",
	player = {},
	bed_position = {},
	pos = {},
	spawn = {},
	respawn = {},
	day_interval = {start = 0.2, finish = 0.805},
	get_translator = S,
	formspec = "size[8,11;true]"
	.. "no_prepend[]"
	.. "bgcolor[#080808BB;true]"
	.. "button_exit[2,10;4,0.75;leave;" .. minetest.formspec_escape(S("Leave Bed")) .. "]",
	path = minetest.get_modpath("beds")
}



dofile(beds.path .. "/functions.lua")
dofile(beds.path .. "/api.lua")
dofile(beds.path .. "/beds.lua")
dofile(beds.path .. "/spawns.lua")


print("[MOD] Beds loaded")
