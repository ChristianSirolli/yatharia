
appliances = {
    path = minetest.get_modpath(minetest.get_current_modname()),
    translator = minetest.get_translator("appliances"),
    have_mesecons = (minetest.global_exists("mesecons")) or (minetest.global_exists("hades_mesecons")),
    have_pipeworks = minetest.global_exists("pipeworks"),
    have_technic = (minetest.global_exists("technic")) or (minetest.global_exists("hades_technic")),
    have_unified = minetest.global_exists("unified_inventory"),
    have_craftguide = (minetest.global_exists("craftguide")) or (minetest.global_exists("hades_craftguide2")),
    have_i3 = minetest.global_exists("i3"),
    have_tt = minetest.global_exists("tt"),
}

dofile(appliances.path.."/functions.lua");
dofile(appliances.path.."/appliance.lua");
dofile(appliances.path.."/tool.lua");
dofile(appliances.path.."/extensions.lua");

