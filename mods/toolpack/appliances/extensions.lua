
appliances.all_extensions = {}

-- supply by power
dofile(appliances.path.."/power_supply.lua");
-- supply by items
dofile(appliances.path.."/item_supply.lua");
-- general supply input (liquid etc.)
dofile(appliances.path.."/supply.lua");
-- control
dofile(appliances.path.."/control.lua");
-- supply by battery (for tools)
dofile(appliances.path.."/battery_supply.lua");

