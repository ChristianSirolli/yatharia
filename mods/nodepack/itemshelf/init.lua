-- Itemshelf mod by Zorman2000
itemshelf = {
    path = minetest.get_modpath("itemshelf")
}

-- Load files
dofile(itemshelf.path .. "/api.lua")
dofile(itemshelf.path .. "/nodes.lua")
dofile(itemshelf.path .. "/recipes.lua") --from nac (minetest4kids)
