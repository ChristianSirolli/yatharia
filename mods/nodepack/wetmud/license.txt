LICENSING
---------
The code is available under: GNU GPL v3.0 or later
Offline copy: gpl-3.0.txt
Online copy: https://www.gnu.org/licenses/gpl-3.0.html

The (original) texture is available under: CC-BY 4.0
Offline copy: cc-by-4.0.txt
Online copy: https://creativecommons.org/licenses/by/4.0/
