
composting = {
  translator = minetest.get_translator("composting"),
  path = minetest.get_modpath(minetest.get_current_modname())
}

dofile(composting.path.."/settings.lua")

dofile(composting.path.."/composter.lua")
if minetest.global_exists("appliances") then
  dofile(composting.path.."/electric_composter.lua")
end

dofile(composting.path.."/integration.lua")

dofile(composting.path.."/garden_soil.lua")

dofile(composting.path.."/craftitems.lua")
dofile(composting.path.."/crafting.lua")
