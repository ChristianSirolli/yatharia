--[[
		Minetest-mod "Chains", Adds chains to climb or decorate
		Copyright (C) 2022 J. A. Anders

		This program is free software; you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation; version 3 of the License.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
		MA 02110-1301, USA.
]]

--
-- Mod Version 0.1
--

chains = {}

-- Get Translator
local S = minetest.get_translator("chains")
chains.get_translator = S
local S = chains.get_translator

-- Chain's hitbox

local chain_hitbox = { -2/16, -8/16, -2/16, 2/16, 8/16, 2/16}

-- The chains

minetest.register_node("chains:chain_steel", {
	description = S("Steel Chain"),
	inventory_image = "chains_chain_steel.png",
	tiles = {"chains_chain_steel.png"},
	groups = {cracky=1, level=2, chain=1},
	drawtype = "plantlike",
	walkable = false,
	climbable = true,
	selection_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	node_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	sounds = default.node_sound_metal_defaults(),
})

minetest.register_node("chains:chain_bronze", {
	description = S("Bronze Chain"),
	inventory_image = "chains_chain_bronze.png",
	tiles = {"chains_chain_bronze.png"},
	groups = {cracky=1, level=2, chain=1},
	drawtype = "plantlike",
	walkable = false,
	climbable = true,
	selection_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	node_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	sounds = default.node_sound_metal_defaults(),
})

minetest.register_node("chains:chain_gold", {
	description = S("Goldened Chain"),
	inventory_image = "chains_chain_gold.png",
	tiles = {"chains_chain_gold.png"},
	groups = {cracky=1, level=2, chain=1},
	drawtype = "plantlike",
	walkable = false,
	climbable = true,
	selection_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	node_box = {
		type = "fixed",
		fixed = chain_hitbox
	},
	sounds = default.node_sound_metal_defaults(),
})



-- The chain links

minetest.register_craftitem("chains:chain_links_steel", {
  description = S("Steel Chain Links"),
  inventory_image = "chains_chain_links_steel.png",
})

minetest.register_craftitem("chains:chain_links_bronze", {
  description = S("Bronze Chain Links"),
  inventory_image = "chains_chain_links_bronze.png",
})


-- Crafting

minetest.register_craft({
  type = "shaped",
  output = "chains:chain_links_steel 9",
  recipe = {
    {"","default:steel_ingot",""},
    {"default:steel_ingot","","default:steel_ingot"},
    {"","default:steel_ingot",""},
  },
})

minetest.register_craft({
  type = "shaped",
  output = "chains:chain_links_bronze 9",
  recipe = {
    {"","default:bronze_ingot",""},
    {"default:bronze_ingot","","default:bronze_ingot"},
    {"","default:bronze_ingot",""},
  },
})

minetest.register_craft({
  type = "shaped",
  output = "chains:chain_steel 2",
  recipe = {
    {"chains:chain_links_steel"},
    {"chains:chain_links_steel"},
    {"chains:chain_links_steel"},
  },
})

minetest.register_craft({
  type = "shaped",
  output = "chains:chain_bronze 2",
  recipe = {
    {"chains:chain_links_bronze"},
    {"chains:chain_links_bronze"},
    {"chains:chain_links_bronze"},
  },
})

minetest.register_craft({
  type = "shaped",
  output = "chains:chain_gold 8",
  recipe = {
    {"group:chain","group:chain","group:chain"},
    {"group:chain","default:gold_ingot","group:chain"},
    {"group:chain","group:chain","group:chain"},
  },
})