palisade = {
  translator = minetest.get_translator("palisade"),
  path = minetest.get_modpath(minetest.get_current_modname()),
  enabled_trees = minetest.settings:get("palisade_enabled_trees") or "*"
}

dofile(palisade.path.."/trees.lua")
dofile(palisade.path.."/palisade.lua")
