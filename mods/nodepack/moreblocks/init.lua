--[[
=====================================================================
** More Blocks **
By Calinou, with the help of ShadowNinja and VanessaE.

Copyright © 2011-2020 Hugo Locurcio and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
=====================================================================
--]]

moreblocks = {
    path = minetest.get_modpath("moreblocks"),
    S = minetest.get_translator("moreblocks")
}

dofile(moreblocks.path .. "/config.lua")
dofile(moreblocks.path .. "/sounds.lua")
dofile(moreblocks.path .. "/circular_saw.lua")
dofile(moreblocks.path .. "/stairsplus/init.lua")

if minetest.global_exists("default") then
    dofile(moreblocks.path .. "/nodes.lua")
    dofile(moreblocks.path .. "/redefinitions.lua")
    dofile(moreblocks.path .. "/crafting.lua")
    dofile(moreblocks.path .. "/aliases.lua")
end
