-- Basic materials mod
-- by Vanessa Dannenberg

-- This mod supplies all those little random craft items that everyone always
-- seems to need, such as metal bars (ala rebar), plastic, wire, and so on.

basic_materials = {
    mod = { author = "Vanessa Dannenberg" },
    path = minetest.get_modpath("basic_materials"),
}

dofile(basic_materials.path .. "/nodes.lua")
dofile(basic_materials.path .. "/craftitems.lua")
dofile(basic_materials.path .. "/crafts.lua")
dofile(basic_materials.path .. "/aliases.lua")