--[[
	Fallen Nodes - Adds dirt, cobble, snow, straw and cactus nodes to
	the	falling_node group. Papyrus will fall too.
	Copyright © 2018, 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]

--
-- Global mod's namespace
--

fallen_nodes = {
	disableSnowBlocks = minetest.settings:get_bool('fallen_nodes_snow'),
	path = minetest.get_modpath('fallen_nodes'),
	mods = {
		darkage = minetest.get_modpath('darkage'),
		default = default.path,
	},
}

function fallen_nodes.TurnToFalling(node_name, def)
	if def == nil then
		def = minetest.registered_nodes[node_name]
	end
	def.groups.falling_node = 1
	minetest.override_item(node_name, {
		groups = def.groups
	})
end

local function Set (list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

local darkageNodes = Set {
	'darkage:chalk',
	'darkage:basalt_rubble',
	'darkage:cobble_with_plaster',
	'darkage:darkdirt',
	'darkage:gneiss_rubble',
	'darkage:mud',
	'darkage:ors_rubble',
	'darkage:silt',
	'darkage:slate_rubble',
}

minetest.register_on_mods_loaded(function()
    for node_name, def in pairs(minetest.registered_nodes) do
		if def.groups and ((def.groups.soil and def.groups.soil > 0) or (def.groups.sand and def.groups.sand > 0) or string.find(node_name, 'cobble') or string.find(node_name, 'straw') or (fallen_nodes.disableSnowBlocks and string.find(node_name, 'snowblock')) or (fallen_nodes.mods.darkage and string.find(node_name, '^darkage:') and darkageNodes[node_name])) then
			fallen_nodes.TurnToFalling(node_name, def)
		end
	end
	if fallen_nodes.mods.default then
		minetest.override_item('default:papyrus', {
			groups = {
				snappy = 3, flammable = 2, attached_node = 1
			}
		})
		minetest.override_item('default:cactus', {
			on_place = minetest.item_place,
			after_place_node = function(pos, oldnode, oldmeta, drops)
				-- only fall if being placed, won't fall if activated while attached to cactus "branch"
				minetest.spawn_falling_node(pos)
			end,
		})
	end
end)

--
-- Minetest engine debug logging
--
minetest.log('action', '[Mod] Fallen Nodes [v1.7.0] loaded.')
