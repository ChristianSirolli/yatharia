# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.

## [1.7.1] - 2022-12-14

	- Fixed cactus nodes not falling because `default` mod set on_place to minetest.rotate_node, which in turn calls a function which calls a function that prevents after_place_node from being ran. So cactus nodes don't rotate when placed anymore.

## [1.7.0] - 2022-12-12

	- Changed cactus nodes to only drop if being placed, not if activated. Prevents cactus plants from falling apart when touched.
	- Added .cdb.json
	- Simplified README.md
	- Added API section in README.md

## [1.6.0] - 2022-12-11

	- Simplified code
	- Removed deprecated files
	- Made all dependencies optional

## [1.5.1] - 2022-12-01

	- Fixed `falling_node` group being set incorrectly, which resulted in a runtime error.

## [1.5.0] - 2019-12-17
### Added

	- All of the Minetest Game's v5.1.0 dirt, cobble, snow, and straw nodes.
	- Option to turn off snow blocks' overriding: Options -> All Options -> Mods -> fallen_nodes
	- Global function to allow third party developers to add their own nodes: fallen_nodes.TurnToFalling("mod_name:node_name")

### Modified

	- Code rewritten from scratch.
	
	

## [1.4.2] - 2019-10-23
### Modified

	- License changed to EUPL v1.2
	- mod.conf set to follow MT v5.x specifics.
	- Minor code improvements.


## [1.4.1] - 2018-05-21
### Modified

	- Mod rearranged to follow Minetest-Mods manifesto's guidelines.

### Removed

	- ../doc/


## [1.4.0] - 2018-04-25
### Added

	- Support for "Landscape" v0.1 by BlockMen

### Changed

	- Corrected a typo preventing to load ../darkage.lua

### Removed

	- Development debug info displayed in the console.


## [1.3.2] - 2018-04-20
### Added

	- ../doc/

### Changed

	- Node overriders now use the "for" cycle, where possible, this makes
		easier to read and mantain the code.
