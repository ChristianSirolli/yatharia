**_Adds dirt, cobble, straw and cactus nodes to the falling_node group. Papyrus will fall too._**

Dirt, cobble, straw and cactus nodes nodes will no longer 'float'.
Papyrus nodes will fall as well.

Forked from Hamlet's abandoned [repo](https://codeberg.org/Hamlet/fallen_nodes).

**License:** [EUPL v1.2](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863) or later.

**Supported:** default, stairs, farming (found [Minetest Game](https://github.com/minetest/minetest_game)), [Landscape](https://forum.minetest.net/viewtopic.php?t=5190), [Experimental Mapgen](https://content.minetest.net/packages/Nore/mg/), [Darkage](https://content.minetest.net/packages/addi/darkage/) (Addi's fork)

**Advanced setting:** Settings -> All Settings -> Mods -> fallen_nodes

**API**

If you want to change certain nodes to fall, either define or override their definition by setting this group:
```
falling_node = 1
```
Or depend on this mod and use this function:
```
fallen_nodes.TurnToFalling(node_name, def)
```
Set the node name in the first argument, and optionally provide the node definition in the second argument. If you don't, this mod will look it up for you.
