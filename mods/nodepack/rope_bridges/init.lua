
rope_bridges = {
  translator = minetest.get_translator("rope_bridges"),
  path = minetest.get_modpath(minetest.get_current_modname())
}

-- settings
rope_bridges.bridge_body_waving = minetest.settings:get_bool("rope_bridges_body_waving", true)
rope_bridges.enabled_ropes = minetest.settings:get("rope_bridges_enabled_ropes") or "*"
rope_bridges.enabled_woods = minetest.settings:get("rope_bridges_enabled_woods") or "*"

dofile(rope_bridges.path.."/ropes.lua")
dofile(rope_bridges.path.."/woods.lua")

dofile(rope_bridges.path.."/planks.lua")

